<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToRefFamilyStatusLangsTable extends Migration
{
    public function up()
    {
        Schema::table('ref_family_status_langs', function (Blueprint $table) {
            $table->unsignedBigInteger('language_id')->nullable();
            $table->foreign('language_id', 'language_fk_8345497')->references('id')->on('languages');
            $table->unsignedBigInteger('family_status_id')->nullable();
            $table->foreign('family_status_id', 'family_status_fk_8345498')->references('id')->on('ref_family_statuses');
        });
    }
}
