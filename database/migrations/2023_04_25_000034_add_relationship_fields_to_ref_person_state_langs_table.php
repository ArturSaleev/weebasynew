<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToRefPersonStateLangsTable extends Migration
{
    public function up()
    {
        Schema::table('ref_person_state_langs', function (Blueprint $table) {
            $table->unsignedBigInteger('language_id')->nullable();
            $table->foreign('language_id', 'language_fk_8345543')->references('id')->on('languages');
            $table->unsignedBigInteger('person_state_id')->nullable();
            $table->foreign('person_state_id', 'person_state_fk_8345544')->references('id')->on('ref_person_states');
        });
    }
}
