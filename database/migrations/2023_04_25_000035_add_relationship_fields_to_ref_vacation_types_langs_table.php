<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToRefVacationTypesLangsTable extends Migration
{
    public function up()
    {
        Schema::table('ref_vacation_types_langs', function (Blueprint $table) {
            $table->unsignedBigInteger('language_id')->nullable();
            $table->foreign('language_id', 'language_fk_8345583')->references('id')->on('languages');
            $table->unsignedBigInteger('vacation_type_id')->nullable();
            $table->foreign('vacation_type_id', 'vacation_type_fk_8345584')->references('id')->on('ref_vacation_types');
        });
    }
}
