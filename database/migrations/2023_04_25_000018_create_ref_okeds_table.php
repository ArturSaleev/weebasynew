<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefOkedsTable extends Migration
{
    public function up()
    {
        Schema::create('ref_okeds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('oked')->nullable();
            $table->string('name_oked')->nullable();
            $table->string('name')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
