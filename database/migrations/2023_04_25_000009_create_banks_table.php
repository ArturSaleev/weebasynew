<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBanksTable extends Migration
{
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('short_name')->nullable();
            $table->string('logo')->nullable();
            $table->string('mfo')->nullable();
            $table->string('mfo_head')->nullable();
            $table->string('mfo_rkc')->nullable();
            $table->string('kor_account')->nullable();
            $table->boolean('status')->default(0)->nullable();
            $table->string('bin')->nullable();
            $table->string('bik')->nullable();
            $table->float('commis', 15, 4)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
