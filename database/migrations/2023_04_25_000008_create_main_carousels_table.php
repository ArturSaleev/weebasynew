<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainCarouselsTable extends Migration
{
    public function up()
    {
        Schema::create('main_carousels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->longText('body')->nullable();
            $table->boolean('visible')->default(0)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
