<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToRefOkedLangsTable extends Migration
{
    public function up()
    {
        Schema::table('ref_oked_langs', function (Blueprint $table) {
            $table->unsignedBigInteger('language_id')->nullable();
            $table->foreign('language_id', 'language_fk_8345523')->references('id')->on('languages');
            $table->unsignedBigInteger('oked_id')->nullable();
            $table->foreign('oked_id', 'oked_fk_8345524')->references('id')->on('ref_okeds');
        });
    }
}
