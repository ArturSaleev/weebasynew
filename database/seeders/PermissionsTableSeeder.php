<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'permission_create',
            ],
            [
                'id'    => 3,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'title' => 'permission_show',
            ],
            [
                'id'    => 5,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'title' => 'permission_access',
            ],
            [
                'id'    => 7,
                'title' => 'role_create',
            ],
            [
                'id'    => 8,
                'title' => 'role_edit',
            ],
            [
                'id'    => 9,
                'title' => 'role_show',
            ],
            [
                'id'    => 10,
                'title' => 'role_delete',
            ],
            [
                'id'    => 11,
                'title' => 'role_access',
            ],
            [
                'id'    => 12,
                'title' => 'user_create',
            ],
            [
                'id'    => 13,
                'title' => 'user_edit',
            ],
            [
                'id'    => 14,
                'title' => 'user_show',
            ],
            [
                'id'    => 15,
                'title' => 'user_delete',
            ],
            [
                'id'    => 16,
                'title' => 'user_access',
            ],
            [
                'id'    => 17,
                'title' => 'content_management_access',
            ],
            [
                'id'    => 18,
                'title' => 'content_category_create',
            ],
            [
                'id'    => 19,
                'title' => 'content_category_edit',
            ],
            [
                'id'    => 20,
                'title' => 'content_category_show',
            ],
            [
                'id'    => 21,
                'title' => 'content_category_delete',
            ],
            [
                'id'    => 22,
                'title' => 'content_category_access',
            ],
            [
                'id'    => 23,
                'title' => 'content_tag_create',
            ],
            [
                'id'    => 24,
                'title' => 'content_tag_edit',
            ],
            [
                'id'    => 25,
                'title' => 'content_tag_show',
            ],
            [
                'id'    => 26,
                'title' => 'content_tag_delete',
            ],
            [
                'id'    => 27,
                'title' => 'content_tag_access',
            ],
            [
                'id'    => 28,
                'title' => 'content_page_create',
            ],
            [
                'id'    => 29,
                'title' => 'content_page_edit',
            ],
            [
                'id'    => 30,
                'title' => 'content_page_show',
            ],
            [
                'id'    => 31,
                'title' => 'content_page_delete',
            ],
            [
                'id'    => 32,
                'title' => 'content_page_access',
            ],
            [
                'id'    => 33,
                'title' => 'api_access',
            ],
            [
                'id'    => 34,
                'title' => 'main_carousel_create',
            ],
            [
                'id'    => 35,
                'title' => 'main_carousel_edit',
            ],
            [
                'id'    => 36,
                'title' => 'main_carousel_show',
            ],
            [
                'id'    => 37,
                'title' => 'main_carousel_delete',
            ],
            [
                'id'    => 38,
                'title' => 'main_carousel_access',
            ],
            [
                'id'    => 39,
                'title' => 'bank_create',
            ],
            [
                'id'    => 40,
                'title' => 'bank_edit',
            ],
            [
                'id'    => 41,
                'title' => 'bank_show',
            ],
            [
                'id'    => 42,
                'title' => 'bank_delete',
            ],
            [
                'id'    => 43,
                'title' => 'bank_access',
            ],
            [
                'id'    => 44,
                'title' => 'banks_lang_create',
            ],
            [
                'id'    => 45,
                'title' => 'banks_lang_edit',
            ],
            [
                'id'    => 46,
                'title' => 'banks_lang_show',
            ],
            [
                'id'    => 47,
                'title' => 'banks_lang_delete',
            ],
            [
                'id'    => 48,
                'title' => 'banks_lang_access',
            ],
            [
                'id'    => 49,
                'title' => 'language_create',
            ],
            [
                'id'    => 50,
                'title' => 'language_edit',
            ],
            [
                'id'    => 51,
                'title' => 'language_show',
            ],
            [
                'id'    => 52,
                'title' => 'language_delete',
            ],
            [
                'id'    => 53,
                'title' => 'language_access',
            ],
            [
                'id'    => 54,
                'title' => 'country_create',
            ],
            [
                'id'    => 55,
                'title' => 'country_edit',
            ],
            [
                'id'    => 56,
                'title' => 'country_show',
            ],
            [
                'id'    => 57,
                'title' => 'country_delete',
            ],
            [
                'id'    => 58,
                'title' => 'country_access',
            ],
            [
                'id'    => 59,
                'title' => 'countries_lang_create',
            ],
            [
                'id'    => 60,
                'title' => 'countries_lang_edit',
            ],
            [
                'id'    => 61,
                'title' => 'countries_lang_show',
            ],
            [
                'id'    => 62,
                'title' => 'countries_lang_delete',
            ],
            [
                'id'    => 63,
                'title' => 'countries_lang_access',
            ],
            [
                'id'    => 64,
                'title' => 'ref_family_status_create',
            ],
            [
                'id'    => 65,
                'title' => 'ref_family_status_edit',
            ],
            [
                'id'    => 66,
                'title' => 'ref_family_status_show',
            ],
            [
                'id'    => 67,
                'title' => 'ref_family_status_delete',
            ],
            [
                'id'    => 68,
                'title' => 'ref_family_status_access',
            ],
            [
                'id'    => 69,
                'title' => 'ref_family_status_lang_create',
            ],
            [
                'id'    => 70,
                'title' => 'ref_family_status_lang_edit',
            ],
            [
                'id'    => 71,
                'title' => 'ref_family_status_lang_show',
            ],
            [
                'id'    => 72,
                'title' => 'ref_family_status_lang_delete',
            ],
            [
                'id'    => 73,
                'title' => 'ref_family_status_lang_access',
            ],
            [
                'id'    => 74,
                'title' => 'nationality_create',
            ],
            [
                'id'    => 75,
                'title' => 'nationality_edit',
            ],
            [
                'id'    => 76,
                'title' => 'nationality_show',
            ],
            [
                'id'    => 77,
                'title' => 'nationality_delete',
            ],
            [
                'id'    => 78,
                'title' => 'nationality_access',
            ],
            [
                'id'    => 79,
                'title' => 'nationality_lang_create',
            ],
            [
                'id'    => 80,
                'title' => 'nationality_lang_edit',
            ],
            [
                'id'    => 81,
                'title' => 'nationality_lang_show',
            ],
            [
                'id'    => 82,
                'title' => 'nationality_lang_delete',
            ],
            [
                'id'    => 83,
                'title' => 'nationality_lang_access',
            ],
            [
                'id'    => 84,
                'title' => 'ref_oked_create',
            ],
            [
                'id'    => 85,
                'title' => 'ref_oked_edit',
            ],
            [
                'id'    => 86,
                'title' => 'ref_oked_show',
            ],
            [
                'id'    => 87,
                'title' => 'ref_oked_delete',
            ],
            [
                'id'    => 88,
                'title' => 'ref_oked_access',
            ],
            [
                'id'    => 89,
                'title' => 'ref_oked_lang_create',
            ],
            [
                'id'    => 90,
                'title' => 'ref_oked_lang_edit',
            ],
            [
                'id'    => 91,
                'title' => 'ref_oked_lang_show',
            ],
            [
                'id'    => 92,
                'title' => 'ref_oked_lang_delete',
            ],
            [
                'id'    => 93,
                'title' => 'ref_oked_lang_access',
            ],
            [
                'id'    => 94,
                'title' => 'ref_person_state_create',
            ],
            [
                'id'    => 95,
                'title' => 'ref_person_state_edit',
            ],
            [
                'id'    => 96,
                'title' => 'ref_person_state_show',
            ],
            [
                'id'    => 97,
                'title' => 'ref_person_state_delete',
            ],
            [
                'id'    => 98,
                'title' => 'ref_person_state_access',
            ],
            [
                'id'    => 99,
                'title' => 'ref_person_state_lang_create',
            ],
            [
                'id'    => 100,
                'title' => 'ref_person_state_lang_edit',
            ],
            [
                'id'    => 101,
                'title' => 'ref_person_state_lang_show',
            ],
            [
                'id'    => 102,
                'title' => 'ref_person_state_lang_delete',
            ],
            [
                'id'    => 103,
                'title' => 'ref_person_state_lang_access',
            ],
            [
                'id'    => 104,
                'title' => 'ref_vacation_type_create',
            ],
            [
                'id'    => 105,
                'title' => 'ref_vacation_type_edit',
            ],
            [
                'id'    => 106,
                'title' => 'ref_vacation_type_show',
            ],
            [
                'id'    => 107,
                'title' => 'ref_vacation_type_delete',
            ],
            [
                'id'    => 108,
                'title' => 'ref_vacation_type_access',
            ],
            [
                'id'    => 109,
                'title' => 'ref_vacation_types_lang_create',
            ],
            [
                'id'    => 110,
                'title' => 'ref_vacation_types_lang_edit',
            ],
            [
                'id'    => 111,
                'title' => 'ref_vacation_types_lang_show',
            ],
            [
                'id'    => 112,
                'title' => 'ref_vacation_types_lang_delete',
            ],
            [
                'id'    => 113,
                'title' => 'ref_vacation_types_lang_access',
            ],
            [
                'id'    => 114,
                'title' => 'comment_create',
            ],
            [
                'id'    => 115,
                'title' => 'comment_edit',
            ],
            [
                'id'    => 116,
                'title' => 'comment_show',
            ],
            [
                'id'    => 117,
                'title' => 'comment_delete',
            ],
            [
                'id'    => 118,
                'title' => 'comment_access',
            ],
            [
                'id'    => 119,
                'title' => 'profile_password_edit',
            ],
        ];

        Permission::insert($permissions);
    }
}
