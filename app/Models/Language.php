<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Language extends Model
{
    use SoftDeletes, HasFactory;

    public $table = 'languages';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'short',
        'logo',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function languageBanksLangs()
    {
        return $this->hasMany(BanksLang::class, 'language_id', 'id');
    }

    public function languageCountriesLangs()
    {
        return $this->hasMany(CountriesLang::class, 'language_id', 'id');
    }

    public function languageRefFamilyStatusLangs()
    {
        return $this->hasMany(RefFamilyStatusLang::class, 'language_id', 'id');
    }

    public function languageNationalityLangs()
    {
        return $this->hasMany(NationalityLang::class, 'language_id', 'id');
    }

    public function languageRefOkedLangs()
    {
        return $this->hasMany(RefOkedLang::class, 'language_id', 'id');
    }

    public function languageRefPersonStateLangs()
    {
        return $this->hasMany(RefPersonStateLang::class, 'language_id', 'id');
    }

    public function languageRefVacationTypesLangs()
    {
        return $this->hasMany(RefVacationTypesLang::class, 'language_id', 'id');
    }
}
