<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RefPersonStateLang extends Model
{
    use SoftDeletes, HasFactory;

    public $table = 'ref_person_state_langs';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'language_id',
        'person_state_id',
        'name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function language()
    {
        return $this->belongsTo(Language::class, 'language_id');
    }

    public function person_state()
    {
        return $this->belongsTo(RefPersonState::class, 'person_state_id');
    }
}
