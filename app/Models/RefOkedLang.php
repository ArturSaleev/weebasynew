<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RefOkedLang extends Model
{
    use SoftDeletes, HasFactory;

    public $table = 'ref_oked_langs';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'language_id',
        'oked_id',
        'name_oked',
        'name',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function language()
    {
        return $this->belongsTo(Language::class, 'language_id');
    }

    public function oked()
    {
        return $this->belongsTo(RefOked::class, 'oked_id');
    }
}
