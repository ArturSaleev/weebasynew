<?php


namespace App\Helpers;


use App\Models\ContentCategory;
use App\Models\ContentPage;
use App\Models\ContentTag;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class MainHelper
{
    public static function MenuCategoryes(): Collection|array
    {
        $ids = DB::table("content_category_content_page")->pluck('content_category_id');
        return ContentCategory::query()->whereIn('id', $ids)->get();
    }

    public static function MenuTags(): Collection|array
    {
        $ids = DB::table("content_page_content_tag")->pluck('content_tag_id');
        return ContentTag::query()->whereIn('id', $ids)->get();
    }
}
