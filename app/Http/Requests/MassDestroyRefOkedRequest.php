<?php

namespace App\Http\Requests;

use App\Models\RefOked;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyRefOkedRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('ref_oked_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:ref_okeds,id',
        ];
    }
}
