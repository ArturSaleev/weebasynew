<?php

namespace App\Http\Requests;

use App\Models\RefVacationTypesLang;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyRefVacationTypesLangRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('ref_vacation_types_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:ref_vacation_types_langs,id',
        ];
    }
}
