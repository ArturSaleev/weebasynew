<?php

namespace App\Http\Requests;

use App\Models\BanksLang;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyBanksLangRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('banks_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:banks_langs,id',
        ];
    }
}
