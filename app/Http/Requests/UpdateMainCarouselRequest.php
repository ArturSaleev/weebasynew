<?php

namespace App\Http\Requests;

use App\Models\MainCarousel;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateMainCarouselRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('main_carousel_edit');
    }

    public function rules()
    {
        return [
            'title' => [
                'string',
                'nullable',
            ],
        ];
    }
}
