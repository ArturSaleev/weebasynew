<?php

namespace App\Http\Requests;

use App\Models\CountriesLang;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateCountriesLangRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('countries_lang_edit');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'nullable',
            ],
        ];
    }
}
