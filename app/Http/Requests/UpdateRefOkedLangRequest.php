<?php

namespace App\Http\Requests;

use App\Models\RefOkedLang;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateRefOkedLangRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('ref_oked_lang_edit');
    }

    public function rules()
    {
        return [
            'name_oked' => [
                'string',
                'nullable',
            ],
            'name' => [
                'string',
                'nullable',
            ],
        ];
    }
}
