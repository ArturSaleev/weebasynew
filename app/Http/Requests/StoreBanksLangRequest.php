<?php

namespace App\Http\Requests;

use App\Models\BanksLang;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreBanksLangRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('banks_lang_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'nullable',
            ],
        ];
    }
}
