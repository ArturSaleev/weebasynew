<?php

namespace App\Http\Requests;

use App\Models\RefFamilyStatusLang;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreRefFamilyStatusLangRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('ref_family_status_lang_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'nullable',
            ],
        ];
    }
}
