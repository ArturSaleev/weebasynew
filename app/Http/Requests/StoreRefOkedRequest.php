<?php

namespace App\Http\Requests;

use App\Models\RefOked;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreRefOkedRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('ref_oked_create');
    }

    public function rules()
    {
        return [
            'oked' => [
                'string',
                'nullable',
            ],
            'name_oked' => [
                'string',
                'nullable',
            ],
            'name' => [
                'string',
                'nullable',
            ],
        ];
    }
}
