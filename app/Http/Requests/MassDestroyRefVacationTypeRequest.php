<?php

namespace App\Http\Requests;

use App\Models\RefVacationType;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyRefVacationTypeRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('ref_vacation_type_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:ref_vacation_types,id',
        ];
    }
}
