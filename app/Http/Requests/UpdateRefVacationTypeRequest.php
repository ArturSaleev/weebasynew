<?php

namespace App\Http\Requests;

use App\Models\RefVacationType;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateRefVacationTypeRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('ref_vacation_type_edit');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'nullable',
            ],
            'symbol' => [
                'string',
                'nullable',
            ],
            'type' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
