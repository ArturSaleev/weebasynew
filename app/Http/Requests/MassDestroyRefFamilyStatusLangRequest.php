<?php

namespace App\Http\Requests;

use App\Models\RefFamilyStatusLang;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyRefFamilyStatusLangRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('ref_family_status_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:ref_family_status_langs,id',
        ];
    }
}
