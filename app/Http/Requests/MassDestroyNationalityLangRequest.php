<?php

namespace App\Http\Requests;

use App\Models\NationalityLang;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyNationalityLangRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('nationality_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:nationality_langs,id',
        ];
    }
}
