<?php

namespace App\Http\Requests;

use App\Models\CountriesLang;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyCountriesLangRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('countries_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:countries_langs,id',
        ];
    }
}
