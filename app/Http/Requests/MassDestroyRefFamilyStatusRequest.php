<?php

namespace App\Http\Requests;

use App\Models\RefFamilyStatus;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyRefFamilyStatusRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('ref_family_status_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:ref_family_statuses,id',
        ];
    }
}
