<?php

namespace App\Http\Requests;

use App\Models\RefOkedLang;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyRefOkedLangRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('ref_oked_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:ref_oked_langs,id',
        ];
    }
}
