<?php

namespace App\Http\Requests;

use App\Models\CountriesLang;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreCountriesLangRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('countries_lang_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'nullable',
            ],
        ];
    }
}
