<?php

namespace App\Http\Requests;

use App\Models\RefPersonState;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyRefPersonStateRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('ref_person_state_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:ref_person_states,id',
        ];
    }
}
