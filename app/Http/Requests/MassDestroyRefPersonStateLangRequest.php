<?php

namespace App\Http\Requests;

use App\Models\RefPersonStateLang;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyRefPersonStateLangRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('ref_person_state_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:ref_person_state_langs,id',
        ];
    }
}
