<?php

namespace App\Http\Requests;

use App\Models\RefVacationTypesLang;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreRefVacationTypesLangRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('ref_vacation_types_lang_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'nullable',
            ],
        ];
    }
}
