<?php

namespace App\Http\Requests;

use App\Models\RefFamilyStatus;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateRefFamilyStatusRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('ref_family_status_edit');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'nullable',
            ],
        ];
    }
}
