<?php

namespace App\Http\Requests;

use App\Models\RefPersonState;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateRefPersonStateRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('ref_person_state_edit');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'nullable',
            ],
        ];
    }
}
