<?php

namespace App\Http\Requests;

use App\Models\RefPersonStateLang;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreRefPersonStateLangRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('ref_person_state_lang_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'nullable',
            ],
        ];
    }
}
