<?php

namespace App\Http\Requests;

use App\Models\Bank;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreBankRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('bank_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'nullable',
            ],
            'short_name' => [
                'string',
                'nullable',
            ],
            'logo' => [
                'string',
                'nullable',
            ],
            'mfo' => [
                'string',
                'nullable',
            ],
            'mfo_head' => [
                'string',
                'nullable',
            ],
            'mfo_rkc' => [
                'string',
                'nullable',
            ],
            'kor_account' => [
                'string',
                'nullable',
            ],
            'bin' => [
                'string',
                'nullable',
            ],
            'bik' => [
                'string',
                'nullable',
            ],
            'commis' => [
                'numeric',
                'min:0',
            ],
        ];
    }
}
