<?php

namespace App\Http\Requests;

use App\Models\NationalityLang;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateNationalityLangRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('nationality_lang_edit');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'nullable',
            ],
        ];
    }
}
