<?php

namespace App\Http\Requests;

use App\Models\RefVacationTypesLang;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateRefVacationTypesLangRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('ref_vacation_types_lang_edit');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'nullable',
            ],
        ];
    }
}
