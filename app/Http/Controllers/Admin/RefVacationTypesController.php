<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyRefVacationTypeRequest;
use App\Http\Requests\StoreRefVacationTypeRequest;
use App\Http\Requests\UpdateRefVacationTypeRequest;
use App\Models\RefVacationType;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RefVacationTypesController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ref_vacation_type_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refVacationTypes = RefVacationType::all();

        return view('admin.refVacationTypes.index', compact('refVacationTypes'));
    }

    public function create()
    {
        abort_if(Gate::denies('ref_vacation_type_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.refVacationTypes.create');
    }

    public function store(StoreRefVacationTypeRequest $request)
    {
        $refVacationType = RefVacationType::create($request->all());

        return redirect()->route('admin.ref-vacation-types.index');
    }

    public function edit(RefVacationType $refVacationType)
    {
        abort_if(Gate::denies('ref_vacation_type_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.refVacationTypes.edit', compact('refVacationType'));
    }

    public function update(UpdateRefVacationTypeRequest $request, RefVacationType $refVacationType)
    {
        $refVacationType->update($request->all());

        return redirect()->route('admin.ref-vacation-types.index');
    }

    public function show(RefVacationType $refVacationType)
    {
        abort_if(Gate::denies('ref_vacation_type_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refVacationType->load('vacationTypeRefVacationTypesLangs');

        return view('admin.refVacationTypes.show', compact('refVacationType'));
    }

    public function destroy(RefVacationType $refVacationType)
    {
        abort_if(Gate::denies('ref_vacation_type_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refVacationType->delete();

        return back();
    }

    public function massDestroy(MassDestroyRefVacationTypeRequest $request)
    {
        $refVacationTypes = RefVacationType::find(request('ids'));

        foreach ($refVacationTypes as $refVacationType) {
            $refVacationType->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
