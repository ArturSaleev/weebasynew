<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyMainCarouselRequest;
use App\Http\Requests\StoreMainCarouselRequest;
use App\Http\Requests\UpdateMainCarouselRequest;
use App\Models\MainCarousel;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class MainCarouselController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('main_carousel_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $mainCarousels = MainCarousel::all();

        return view('admin.mainCarousels.index', compact('mainCarousels'));
    }

    public function create()
    {
        abort_if(Gate::denies('main_carousel_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.mainCarousels.create');
    }

    public function store(StoreMainCarouselRequest $request)
    {
        $mainCarousel = MainCarousel::create($request->all());

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $mainCarousel->id]);
        }

        return redirect()->route('admin.main-carousels.index');
    }

    public function edit(MainCarousel $mainCarousel)
    {
        abort_if(Gate::denies('main_carousel_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.mainCarousels.edit', compact('mainCarousel'));
    }

    public function update(UpdateMainCarouselRequest $request, MainCarousel $mainCarousel)
    {
        $mainCarousel->update($request->all());

        return redirect()->route('admin.main-carousels.index');
    }

    public function show(MainCarousel $mainCarousel)
    {
        abort_if(Gate::denies('main_carousel_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.mainCarousels.show', compact('mainCarousel'));
    }

    public function destroy(MainCarousel $mainCarousel)
    {
        abort_if(Gate::denies('main_carousel_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $mainCarousel->delete();

        return back();
    }

    public function massDestroy(MassDestroyMainCarouselRequest $request)
    {
        $mainCarousels = MainCarousel::find(request('ids'));

        foreach ($mainCarousels as $mainCarousel) {
            $mainCarousel->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('main_carousel_create') && Gate::denies('main_carousel_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new MainCarousel();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
