<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyRefVacationTypesLangRequest;
use App\Http\Requests\StoreRefVacationTypesLangRequest;
use App\Http\Requests\UpdateRefVacationTypesLangRequest;
use App\Models\Language;
use App\Models\RefVacationType;
use App\Models\RefVacationTypesLang;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RefVacationTypesLangController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ref_vacation_types_lang_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refVacationTypesLangs = RefVacationTypesLang::with(['language', 'vacation_type'])->get();

        return view('admin.refVacationTypesLangs.index', compact('refVacationTypesLangs'));
    }

    public function create()
    {
        abort_if(Gate::denies('ref_vacation_types_lang_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $languages = Language::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $vacation_types = RefVacationType::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.refVacationTypesLangs.create', compact('languages', 'vacation_types'));
    }

    public function store(StoreRefVacationTypesLangRequest $request)
    {
        $refVacationTypesLang = RefVacationTypesLang::create($request->all());

        return redirect()->route('admin.ref-vacation-types-langs.index');
    }

    public function edit(RefVacationTypesLang $refVacationTypesLang)
    {
        abort_if(Gate::denies('ref_vacation_types_lang_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $languages = Language::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $vacation_types = RefVacationType::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $refVacationTypesLang->load('language', 'vacation_type');

        return view('admin.refVacationTypesLangs.edit', compact('languages', 'refVacationTypesLang', 'vacation_types'));
    }

    public function update(UpdateRefVacationTypesLangRequest $request, RefVacationTypesLang $refVacationTypesLang)
    {
        $refVacationTypesLang->update($request->all());

        return redirect()->route('admin.ref-vacation-types-langs.index');
    }

    public function show(RefVacationTypesLang $refVacationTypesLang)
    {
        abort_if(Gate::denies('ref_vacation_types_lang_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refVacationTypesLang->load('language', 'vacation_type');

        return view('admin.refVacationTypesLangs.show', compact('refVacationTypesLang'));
    }

    public function destroy(RefVacationTypesLang $refVacationTypesLang)
    {
        abort_if(Gate::denies('ref_vacation_types_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refVacationTypesLang->delete();

        return back();
    }

    public function massDestroy(MassDestroyRefVacationTypesLangRequest $request)
    {
        $refVacationTypesLangs = RefVacationTypesLang::find(request('ids'));

        foreach ($refVacationTypesLangs as $refVacationTypesLang) {
            $refVacationTypesLang->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
