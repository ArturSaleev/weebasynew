<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyRefPersonStateRequest;
use App\Http\Requests\StoreRefPersonStateRequest;
use App\Http\Requests\UpdateRefPersonStateRequest;
use App\Models\RefPersonState;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RefPersonStateController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ref_person_state_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refPersonStates = RefPersonState::all();

        return view('admin.refPersonStates.index', compact('refPersonStates'));
    }

    public function create()
    {
        abort_if(Gate::denies('ref_person_state_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.refPersonStates.create');
    }

    public function store(StoreRefPersonStateRequest $request)
    {
        $refPersonState = RefPersonState::create($request->all());

        return redirect()->route('admin.ref-person-states.index');
    }

    public function edit(RefPersonState $refPersonState)
    {
        abort_if(Gate::denies('ref_person_state_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.refPersonStates.edit', compact('refPersonState'));
    }

    public function update(UpdateRefPersonStateRequest $request, RefPersonState $refPersonState)
    {
        $refPersonState->update($request->all());

        return redirect()->route('admin.ref-person-states.index');
    }

    public function show(RefPersonState $refPersonState)
    {
        abort_if(Gate::denies('ref_person_state_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refPersonState->load('personStateRefPersonStateLangs');

        return view('admin.refPersonStates.show', compact('refPersonState'));
    }

    public function destroy(RefPersonState $refPersonState)
    {
        abort_if(Gate::denies('ref_person_state_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refPersonState->delete();

        return back();
    }

    public function massDestroy(MassDestroyRefPersonStateRequest $request)
    {
        $refPersonStates = RefPersonState::find(request('ids'));

        foreach ($refPersonStates as $refPersonState) {
            $refPersonState->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
