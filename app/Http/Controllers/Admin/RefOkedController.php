<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyRefOkedRequest;
use App\Http\Requests\StoreRefOkedRequest;
use App\Http\Requests\UpdateRefOkedRequest;
use App\Models\RefOked;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RefOkedController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ref_oked_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refOkeds = RefOked::all();

        return view('admin.refOkeds.index', compact('refOkeds'));
    }

    public function create()
    {
        abort_if(Gate::denies('ref_oked_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.refOkeds.create');
    }

    public function store(StoreRefOkedRequest $request)
    {
        $refOked = RefOked::create($request->all());

        return redirect()->route('admin.ref-okeds.index');
    }

    public function edit(RefOked $refOked)
    {
        abort_if(Gate::denies('ref_oked_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.refOkeds.edit', compact('refOked'));
    }

    public function update(UpdateRefOkedRequest $request, RefOked $refOked)
    {
        $refOked->update($request->all());

        return redirect()->route('admin.ref-okeds.index');
    }

    public function show(RefOked $refOked)
    {
        abort_if(Gate::denies('ref_oked_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refOked->load('okedRefOkedLangs');

        return view('admin.refOkeds.show', compact('refOked'));
    }

    public function destroy(RefOked $refOked)
    {
        abort_if(Gate::denies('ref_oked_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refOked->delete();

        return back();
    }

    public function massDestroy(MassDestroyRefOkedRequest $request)
    {
        $refOkeds = RefOked::find(request('ids'));

        foreach ($refOkeds as $refOked) {
            $refOked->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
