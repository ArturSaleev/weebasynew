<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyCountriesLangRequest;
use App\Http\Requests\StoreCountriesLangRequest;
use App\Http\Requests\UpdateCountriesLangRequest;
use App\Models\CountriesLang;
use App\Models\Country;
use App\Models\Language;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CountriesLangController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('countries_lang_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $countriesLangs = CountriesLang::with(['language', 'country'])->get();

        return view('admin.countriesLangs.index', compact('countriesLangs'));
    }

    public function create()
    {
        abort_if(Gate::denies('countries_lang_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $languages = Language::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $countries = Country::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.countriesLangs.create', compact('countries', 'languages'));
    }

    public function store(StoreCountriesLangRequest $request)
    {
        $countriesLang = CountriesLang::create($request->all());

        return redirect()->route('admin.countries-langs.index');
    }

    public function edit(CountriesLang $countriesLang)
    {
        abort_if(Gate::denies('countries_lang_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $languages = Language::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $countries = Country::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $countriesLang->load('language', 'country');

        return view('admin.countriesLangs.edit', compact('countries', 'countriesLang', 'languages'));
    }

    public function update(UpdateCountriesLangRequest $request, CountriesLang $countriesLang)
    {
        $countriesLang->update($request->all());

        return redirect()->route('admin.countries-langs.index');
    }

    public function show(CountriesLang $countriesLang)
    {
        abort_if(Gate::denies('countries_lang_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $countriesLang->load('language', 'country');

        return view('admin.countriesLangs.show', compact('countriesLang'));
    }

    public function destroy(CountriesLang $countriesLang)
    {
        abort_if(Gate::denies('countries_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $countriesLang->delete();

        return back();
    }

    public function massDestroy(MassDestroyCountriesLangRequest $request)
    {
        $countriesLangs = CountriesLang::find(request('ids'));

        foreach ($countriesLangs as $countriesLang) {
            $countriesLang->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
