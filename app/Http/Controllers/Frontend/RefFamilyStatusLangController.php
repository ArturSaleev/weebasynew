<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyRefFamilyStatusLangRequest;
use App\Http\Requests\StoreRefFamilyStatusLangRequest;
use App\Http\Requests\UpdateRefFamilyStatusLangRequest;
use App\Models\Language;
use App\Models\RefFamilyStatus;
use App\Models\RefFamilyStatusLang;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RefFamilyStatusLangController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ref_family_status_lang_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refFamilyStatusLangs = RefFamilyStatusLang::with(['language', 'family_status'])->get();

        return view('frontend.refFamilyStatusLangs.index', compact('refFamilyStatusLangs'));
    }

    public function create()
    {
        abort_if(Gate::denies('ref_family_status_lang_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $languages = Language::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $family_statuses = RefFamilyStatus::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('frontend.refFamilyStatusLangs.create', compact('family_statuses', 'languages'));
    }

    public function store(StoreRefFamilyStatusLangRequest $request)
    {
        $refFamilyStatusLang = RefFamilyStatusLang::create($request->all());

        return redirect()->route('frontend.ref-family-status-langs.index');
    }

    public function edit(RefFamilyStatusLang $refFamilyStatusLang)
    {
        abort_if(Gate::denies('ref_family_status_lang_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $languages = Language::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $family_statuses = RefFamilyStatus::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $refFamilyStatusLang->load('language', 'family_status');

        return view('frontend.refFamilyStatusLangs.edit', compact('family_statuses', 'languages', 'refFamilyStatusLang'));
    }

    public function update(UpdateRefFamilyStatusLangRequest $request, RefFamilyStatusLang $refFamilyStatusLang)
    {
        $refFamilyStatusLang->update($request->all());

        return redirect()->route('frontend.ref-family-status-langs.index');
    }

    public function show(RefFamilyStatusLang $refFamilyStatusLang)
    {
        abort_if(Gate::denies('ref_family_status_lang_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refFamilyStatusLang->load('language', 'family_status');

        return view('frontend.refFamilyStatusLangs.show', compact('refFamilyStatusLang'));
    }

    public function destroy(RefFamilyStatusLang $refFamilyStatusLang)
    {
        abort_if(Gate::denies('ref_family_status_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refFamilyStatusLang->delete();

        return back();
    }

    public function massDestroy(MassDestroyRefFamilyStatusLangRequest $request)
    {
        $refFamilyStatusLangs = RefFamilyStatusLang::find(request('ids'));

        foreach ($refFamilyStatusLangs as $refFamilyStatusLang) {
            $refFamilyStatusLang->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
