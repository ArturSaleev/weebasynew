<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyNationalityLangRequest;
use App\Http\Requests\StoreNationalityLangRequest;
use App\Http\Requests\UpdateNationalityLangRequest;
use App\Models\Language;
use App\Models\Nationality;
use App\Models\NationalityLang;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class NationalityLangController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('nationality_lang_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $nationalityLangs = NationalityLang::with(['language', 'nationality'])->get();

        return view('frontend.nationalityLangs.index', compact('nationalityLangs'));
    }

    public function create()
    {
        abort_if(Gate::denies('nationality_lang_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $languages = Language::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $nationalities = Nationality::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('frontend.nationalityLangs.create', compact('languages', 'nationalities'));
    }

    public function store(StoreNationalityLangRequest $request)
    {
        $nationalityLang = NationalityLang::create($request->all());

        return redirect()->route('frontend.nationality-langs.index');
    }

    public function edit(NationalityLang $nationalityLang)
    {
        abort_if(Gate::denies('nationality_lang_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $languages = Language::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $nationalities = Nationality::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $nationalityLang->load('language', 'nationality');

        return view('frontend.nationalityLangs.edit', compact('languages', 'nationalities', 'nationalityLang'));
    }

    public function update(UpdateNationalityLangRequest $request, NationalityLang $nationalityLang)
    {
        $nationalityLang->update($request->all());

        return redirect()->route('frontend.nationality-langs.index');
    }

    public function show(NationalityLang $nationalityLang)
    {
        abort_if(Gate::denies('nationality_lang_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $nationalityLang->load('language', 'nationality');

        return view('frontend.nationalityLangs.show', compact('nationalityLang'));
    }

    public function destroy(NationalityLang $nationalityLang)
    {
        abort_if(Gate::denies('nationality_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $nationalityLang->delete();

        return back();
    }

    public function massDestroy(MassDestroyNationalityLangRequest $request)
    {
        $nationalityLangs = NationalityLang::find(request('ids'));

        foreach ($nationalityLangs as $nationalityLang) {
            $nationalityLang->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
