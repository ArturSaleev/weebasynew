<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyBanksLangRequest;
use App\Http\Requests\StoreBanksLangRequest;
use App\Http\Requests\UpdateBanksLangRequest;
use App\Models\Bank;
use App\Models\BanksLang;
use App\Models\Language;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BanksLangController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('banks_lang_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $banksLangs = BanksLang::with(['language', 'bank'])->get();

        return view('frontend.banksLangs.index', compact('banksLangs'));
    }

    public function create()
    {
        abort_if(Gate::denies('banks_lang_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $languages = Language::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $banks = Bank::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('frontend.banksLangs.create', compact('banks', 'languages'));
    }

    public function store(StoreBanksLangRequest $request)
    {
        $banksLang = BanksLang::create($request->all());

        return redirect()->route('frontend.banks-langs.index');
    }

    public function edit(BanksLang $banksLang)
    {
        abort_if(Gate::denies('banks_lang_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $languages = Language::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $banks = Bank::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $banksLang->load('language', 'bank');

        return view('frontend.banksLangs.edit', compact('banks', 'banksLang', 'languages'));
    }

    public function update(UpdateBanksLangRequest $request, BanksLang $banksLang)
    {
        $banksLang->update($request->all());

        return redirect()->route('frontend.banks-langs.index');
    }

    public function show(BanksLang $banksLang)
    {
        abort_if(Gate::denies('banks_lang_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $banksLang->load('language', 'bank');

        return view('frontend.banksLangs.show', compact('banksLang'));
    }

    public function destroy(BanksLang $banksLang)
    {
        abort_if(Gate::denies('banks_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $banksLang->delete();

        return back();
    }

    public function massDestroy(MassDestroyBanksLangRequest $request)
    {
        $banksLangs = BanksLang::find(request('ids'));

        foreach ($banksLangs as $banksLang) {
            $banksLang->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
