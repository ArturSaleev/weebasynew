<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyRefPersonStateLangRequest;
use App\Http\Requests\StoreRefPersonStateLangRequest;
use App\Http\Requests\UpdateRefPersonStateLangRequest;
use App\Models\Language;
use App\Models\RefPersonState;
use App\Models\RefPersonStateLang;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RefPersonStateLangController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ref_person_state_lang_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refPersonStateLangs = RefPersonStateLang::with(['language', 'person_state'])->get();

        return view('frontend.refPersonStateLangs.index', compact('refPersonStateLangs'));
    }

    public function create()
    {
        abort_if(Gate::denies('ref_person_state_lang_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $languages = Language::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $person_states = RefPersonState::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('frontend.refPersonStateLangs.create', compact('languages', 'person_states'));
    }

    public function store(StoreRefPersonStateLangRequest $request)
    {
        $refPersonStateLang = RefPersonStateLang::create($request->all());

        return redirect()->route('frontend.ref-person-state-langs.index');
    }

    public function edit(RefPersonStateLang $refPersonStateLang)
    {
        abort_if(Gate::denies('ref_person_state_lang_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $languages = Language::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $person_states = RefPersonState::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $refPersonStateLang->load('language', 'person_state');

        return view('frontend.refPersonStateLangs.edit', compact('languages', 'person_states', 'refPersonStateLang'));
    }

    public function update(UpdateRefPersonStateLangRequest $request, RefPersonStateLang $refPersonStateLang)
    {
        $refPersonStateLang->update($request->all());

        return redirect()->route('frontend.ref-person-state-langs.index');
    }

    public function show(RefPersonStateLang $refPersonStateLang)
    {
        abort_if(Gate::denies('ref_person_state_lang_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refPersonStateLang->load('language', 'person_state');

        return view('frontend.refPersonStateLangs.show', compact('refPersonStateLang'));
    }

    public function destroy(RefPersonStateLang $refPersonStateLang)
    {
        abort_if(Gate::denies('ref_person_state_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refPersonStateLang->delete();

        return back();
    }

    public function massDestroy(MassDestroyRefPersonStateLangRequest $request)
    {
        $refPersonStateLangs = RefPersonStateLang::find(request('ids'));

        foreach ($refPersonStateLangs as $refPersonStateLang) {
            $refPersonStateLang->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
