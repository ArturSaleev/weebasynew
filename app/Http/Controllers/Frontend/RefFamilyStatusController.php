<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyRefFamilyStatusRequest;
use App\Http\Requests\StoreRefFamilyStatusRequest;
use App\Http\Requests\UpdateRefFamilyStatusRequest;
use App\Models\RefFamilyStatus;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RefFamilyStatusController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ref_family_status_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refFamilyStatuses = RefFamilyStatus::all();

        return view('frontend.refFamilyStatuses.index', compact('refFamilyStatuses'));
    }

    public function create()
    {
        abort_if(Gate::denies('ref_family_status_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('frontend.refFamilyStatuses.create');
    }

    public function store(StoreRefFamilyStatusRequest $request)
    {
        $refFamilyStatus = RefFamilyStatus::create($request->all());

        return redirect()->route('frontend.ref-family-statuses.index');
    }

    public function edit(RefFamilyStatus $refFamilyStatus)
    {
        abort_if(Gate::denies('ref_family_status_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('frontend.refFamilyStatuses.edit', compact('refFamilyStatus'));
    }

    public function update(UpdateRefFamilyStatusRequest $request, RefFamilyStatus $refFamilyStatus)
    {
        $refFamilyStatus->update($request->all());

        return redirect()->route('frontend.ref-family-statuses.index');
    }

    public function show(RefFamilyStatus $refFamilyStatus)
    {
        abort_if(Gate::denies('ref_family_status_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('frontend.refFamilyStatuses.show', compact('refFamilyStatus'));
    }

    public function destroy(RefFamilyStatus $refFamilyStatus)
    {
        abort_if(Gate::denies('ref_family_status_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refFamilyStatus->delete();

        return back();
    }

    public function massDestroy(MassDestroyRefFamilyStatusRequest $request)
    {
        $refFamilyStatuses = RefFamilyStatus::find(request('ids'));

        foreach ($refFamilyStatuses as $refFamilyStatus) {
            $refFamilyStatus->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
