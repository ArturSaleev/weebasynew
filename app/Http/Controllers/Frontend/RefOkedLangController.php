<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyRefOkedLangRequest;
use App\Http\Requests\StoreRefOkedLangRequest;
use App\Http\Requests\UpdateRefOkedLangRequest;
use App\Models\Language;
use App\Models\RefOked;
use App\Models\RefOkedLang;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RefOkedLangController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ref_oked_lang_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refOkedLangs = RefOkedLang::with(['language', 'oked'])->get();

        return view('frontend.refOkedLangs.index', compact('refOkedLangs'));
    }

    public function create()
    {
        abort_if(Gate::denies('ref_oked_lang_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $languages = Language::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $okeds = RefOked::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('frontend.refOkedLangs.create', compact('languages', 'okeds'));
    }

    public function store(StoreRefOkedLangRequest $request)
    {
        $refOkedLang = RefOkedLang::create($request->all());

        return redirect()->route('frontend.ref-oked-langs.index');
    }

    public function edit(RefOkedLang $refOkedLang)
    {
        abort_if(Gate::denies('ref_oked_lang_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $languages = Language::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $okeds = RefOked::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $refOkedLang->load('language', 'oked');

        return view('frontend.refOkedLangs.edit', compact('languages', 'okeds', 'refOkedLang'));
    }

    public function update(UpdateRefOkedLangRequest $request, RefOkedLang $refOkedLang)
    {
        $refOkedLang->update($request->all());

        return redirect()->route('frontend.ref-oked-langs.index');
    }

    public function show(RefOkedLang $refOkedLang)
    {
        abort_if(Gate::denies('ref_oked_lang_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refOkedLang->load('language', 'oked');

        return view('frontend.refOkedLangs.show', compact('refOkedLang'));
    }

    public function destroy(RefOkedLang $refOkedLang)
    {
        abort_if(Gate::denies('ref_oked_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refOkedLang->delete();

        return back();
    }

    public function massDestroy(MassDestroyRefOkedLangRequest $request)
    {
        $refOkedLangs = RefOkedLang::find(request('ids'));

        foreach ($refOkedLangs as $refOkedLang) {
            $refOkedLang->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
