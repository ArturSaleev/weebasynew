<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\ContentPage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
//    /**
//     * Create a new controller instance.
//     *
//     * @return void
//     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function main()
    {
        $result = ContentPage::query()
            ->where('active', true)
            ->orderByDesc('id')
            ->paginate(10);
        return view('welcome', ['result' => $result]);
    }

    public function tags($id)
    {
        $ids = DB::table("content_page_content_tag")
            ->where('content_tag_id', $id)
            ->pluck('content_page_id');
        $result = ContentPage::with(['categories', 'tags'])
            ->whereIn('id', $ids)
            ->where('active', true)
            ->paginate(10);
        return view('welcome', ['result' => $result]);
    }

    public function category($id)
    {
        $ids = DB::table("content_category_content_page")
            ->where('content_category_id', $id)
            ->pluck('content_page_id');
        $result = ContentPage::with(['categories', 'tags'])
            ->whereIn('id', $ids)
            ->where('active', true)
            ->paginate(10);
        return view('welcome', ['result' => $result]);
    }

    public function page($id)
    {
        $result = ContentPage::with(['pageComments'])->find($id);
        if(!$result){
            abort(404);
        }
        return view('page', ['result' => $result]);
    }

    public function setComment(Request $request)
    {
        $comment = new Comment();
        $comment->user_id = Auth::id();
        $comment->page_id = $request->page_id;
        $comment->comment = $request->body;
        $comment->save();
        return redirect('page/'.$request->page_id);
    }
}
