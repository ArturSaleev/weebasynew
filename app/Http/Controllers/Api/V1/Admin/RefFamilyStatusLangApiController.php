<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRefFamilyStatusLangRequest;
use App\Http\Requests\UpdateRefFamilyStatusLangRequest;
use App\Http\Resources\Admin\RefFamilyStatusLangResource;
use App\Models\RefFamilyStatusLang;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RefFamilyStatusLangApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ref_family_status_lang_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

//        return new RefFamilyStatusLangResource(RefFamilyStatusLang::with(['language', 'family_status'])->get());
        return new RefFamilyStatusLangResource(RefFamilyStatusLang::all());
    }

    public function store(StoreRefFamilyStatusLangRequest $request)
    {
        $refFamilyStatusLang = RefFamilyStatusLang::create($request->all());

        return (new RefFamilyStatusLangResource($refFamilyStatusLang))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(RefFamilyStatusLang $refFamilyStatusLang)
    {
        abort_if(Gate::denies('ref_family_status_lang_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RefFamilyStatusLangResource($refFamilyStatusLang->load(['language', 'family_status']));
    }

    public function update(UpdateRefFamilyStatusLangRequest $request, RefFamilyStatusLang $refFamilyStatusLang)
    {
        $refFamilyStatusLang->update($request->all());

        return (new RefFamilyStatusLangResource($refFamilyStatusLang))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(RefFamilyStatusLang $refFamilyStatusLang)
    {
        abort_if(Gate::denies('ref_family_status_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refFamilyStatusLang->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
