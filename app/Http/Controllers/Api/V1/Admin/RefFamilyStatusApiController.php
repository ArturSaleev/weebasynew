<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRefFamilyStatusRequest;
use App\Http\Requests\UpdateRefFamilyStatusRequest;
use App\Http\Resources\Admin\RefFamilyStatusResource;
use App\Models\RefFamilyStatus;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RefFamilyStatusApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ref_family_status_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RefFamilyStatusResource(RefFamilyStatus::all());
    }

    public function store(StoreRefFamilyStatusRequest $request)
    {
        $refFamilyStatus = RefFamilyStatus::create($request->all());

        return (new RefFamilyStatusResource($refFamilyStatus))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(RefFamilyStatus $refFamilyStatus)
    {
        abort_if(Gate::denies('ref_family_status_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RefFamilyStatusResource($refFamilyStatus);
    }

    public function update(UpdateRefFamilyStatusRequest $request, RefFamilyStatus $refFamilyStatus)
    {
        $refFamilyStatus->update($request->all());

        return (new RefFamilyStatusResource($refFamilyStatus))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(RefFamilyStatus $refFamilyStatus)
    {
        abort_if(Gate::denies('ref_family_status_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refFamilyStatus->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
