<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRefOkedRequest;
use App\Http\Requests\UpdateRefOkedRequest;
use App\Http\Resources\Admin\RefOkedResource;
use App\Models\RefOked;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RefOkedApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ref_oked_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RefOkedResource(RefOked::all());
    }

    public function store(StoreRefOkedRequest $request)
    {
        $refOked = RefOked::create($request->all());

        return (new RefOkedResource($refOked))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(RefOked $refOked)
    {
        abort_if(Gate::denies('ref_oked_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RefOkedResource($refOked);
    }

    public function update(UpdateRefOkedRequest $request, RefOked $refOked)
    {
        $refOked->update($request->all());

        return (new RefOkedResource($refOked))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(RefOked $refOked)
    {
        abort_if(Gate::denies('ref_oked_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refOked->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
