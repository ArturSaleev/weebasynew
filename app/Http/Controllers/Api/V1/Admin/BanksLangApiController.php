<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBanksLangRequest;
use App\Http\Requests\UpdateBanksLangRequest;
use App\Http\Resources\Admin\BanksLangResource;
use App\Models\BanksLang;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BanksLangApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('banks_lang_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new BanksLangResource(BanksLang::with(['language', 'bank'])->get());
    }

    public function store(StoreBanksLangRequest $request)
    {
        $banksLang = BanksLang::create($request->all());

        return (new BanksLangResource($banksLang))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(BanksLang $banksLang)
    {
        abort_if(Gate::denies('banks_lang_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new BanksLangResource($banksLang->load(['language', 'bank']));
    }

    public function update(UpdateBanksLangRequest $request, BanksLang $banksLang)
    {
        $banksLang->update($request->all());

        return (new BanksLangResource($banksLang))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(BanksLang $banksLang)
    {
        abort_if(Gate::denies('banks_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $banksLang->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
