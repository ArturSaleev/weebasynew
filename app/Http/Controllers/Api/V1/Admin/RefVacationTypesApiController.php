<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRefVacationTypeRequest;
use App\Http\Requests\UpdateRefVacationTypeRequest;
use App\Http\Resources\Admin\RefVacationTypeResource;
use App\Models\RefVacationType;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RefVacationTypesApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ref_vacation_type_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RefVacationTypeResource(RefVacationType::all());
    }

    public function store(StoreRefVacationTypeRequest $request)
    {
        $refVacationType = RefVacationType::create($request->all());

        return (new RefVacationTypeResource($refVacationType))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(RefVacationType $refVacationType)
    {
        abort_if(Gate::denies('ref_vacation_type_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RefVacationTypeResource($refVacationType);
    }

    public function update(UpdateRefVacationTypeRequest $request, RefVacationType $refVacationType)
    {
        $refVacationType->update($request->all());

        return (new RefVacationTypeResource($refVacationType))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(RefVacationType $refVacationType)
    {
        abort_if(Gate::denies('ref_vacation_type_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refVacationType->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
