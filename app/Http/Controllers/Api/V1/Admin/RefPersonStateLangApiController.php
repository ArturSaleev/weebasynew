<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRefPersonStateLangRequest;
use App\Http\Requests\UpdateRefPersonStateLangRequest;
use App\Http\Resources\Admin\RefPersonStateLangResource;
use App\Models\RefPersonStateLang;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RefPersonStateLangApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ref_person_state_lang_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RefPersonStateLangResource(RefPersonStateLang::with(['language', 'person_state'])->get());
    }

    public function store(StoreRefPersonStateLangRequest $request)
    {
        $refPersonStateLang = RefPersonStateLang::create($request->all());

        return (new RefPersonStateLangResource($refPersonStateLang))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(RefPersonStateLang $refPersonStateLang)
    {
        abort_if(Gate::denies('ref_person_state_lang_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RefPersonStateLangResource($refPersonStateLang->load(['language', 'person_state']));
    }

    public function update(UpdateRefPersonStateLangRequest $request, RefPersonStateLang $refPersonStateLang)
    {
        $refPersonStateLang->update($request->all());

        return (new RefPersonStateLangResource($refPersonStateLang))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(RefPersonStateLang $refPersonStateLang)
    {
        abort_if(Gate::denies('ref_person_state_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refPersonStateLang->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
