<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRefPersonStateRequest;
use App\Http\Requests\UpdateRefPersonStateRequest;
use App\Http\Resources\Admin\RefPersonStateResource;
use App\Models\RefPersonState;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RefPersonStateApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ref_person_state_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RefPersonStateResource(RefPersonState::all());
    }

    public function store(StoreRefPersonStateRequest $request)
    {
        $refPersonState = RefPersonState::create($request->all());

        return (new RefPersonStateResource($refPersonState))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(RefPersonState $refPersonState)
    {
        abort_if(Gate::denies('ref_person_state_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RefPersonStateResource($refPersonState);
    }

    public function update(UpdateRefPersonStateRequest $request, RefPersonState $refPersonState)
    {
        $refPersonState->update($request->all());

        return (new RefPersonStateResource($refPersonState))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(RefPersonState $refPersonState)
    {
        abort_if(Gate::denies('ref_person_state_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refPersonState->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
