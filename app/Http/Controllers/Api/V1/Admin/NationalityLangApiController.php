<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNationalityLangRequest;
use App\Http\Requests\UpdateNationalityLangRequest;
use App\Http\Resources\Admin\NationalityLangResource;
use App\Models\NationalityLang;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class NationalityLangApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('nationality_lang_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

//        return new NationalityLangResource(NationalityLang::with(['language', 'nationality'])->get());
        return new NationalityLangResource(NationalityLang::all());
    }

    public function store(StoreNationalityLangRequest $request)
    {
        $nationalityLang = NationalityLang::create($request->all());

        return (new NationalityLangResource($nationalityLang))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(NationalityLang $nationalityLang)
    {
        abort_if(Gate::denies('nationality_lang_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new NationalityLangResource($nationalityLang->load(['language', 'nationality']));
    }

    public function update(UpdateNationalityLangRequest $request, NationalityLang $nationalityLang)
    {
        $nationalityLang->update($request->all());

        return (new NationalityLangResource($nationalityLang))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(NationalityLang $nationalityLang)
    {
        abort_if(Gate::denies('nationality_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $nationalityLang->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
