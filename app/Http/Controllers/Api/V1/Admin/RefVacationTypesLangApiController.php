<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRefVacationTypesLangRequest;
use App\Http\Requests\UpdateRefVacationTypesLangRequest;
use App\Http\Resources\Admin\RefVacationTypesLangResource;
use App\Models\RefVacationTypesLang;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RefVacationTypesLangApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ref_vacation_types_lang_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RefVacationTypesLangResource(RefVacationTypesLang::with(['language', 'vacation_type'])->get());
    }

    public function store(StoreRefVacationTypesLangRequest $request)
    {
        $refVacationTypesLang = RefVacationTypesLang::create($request->all());

        return (new RefVacationTypesLangResource($refVacationTypesLang))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(RefVacationTypesLang $refVacationTypesLang)
    {
        abort_if(Gate::denies('ref_vacation_types_lang_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RefVacationTypesLangResource($refVacationTypesLang->load(['language', 'vacation_type']));
    }

    public function update(UpdateRefVacationTypesLangRequest $request, RefVacationTypesLang $refVacationTypesLang)
    {
        $refVacationTypesLang->update($request->all());

        return (new RefVacationTypesLangResource($refVacationTypesLang))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(RefVacationTypesLang $refVacationTypesLang)
    {
        abort_if(Gate::denies('ref_vacation_types_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refVacationTypesLang->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
