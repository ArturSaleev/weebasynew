<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCountriesLangRequest;
use App\Http\Requests\UpdateCountriesLangRequest;
use App\Http\Resources\Admin\CountriesLangResource;
use App\Models\CountriesLang;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CountriesLangApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('countries_lang_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        //return new CountriesLangResource(CountriesLang::with(['language', 'country'])->get());
        return new CountriesLangResource(CountriesLang::all());
    }

    public function store(StoreCountriesLangRequest $request)
    {
        $countriesLang = CountriesLang::create($request->all());

        return (new CountriesLangResource($countriesLang))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(CountriesLang $countriesLang)
    {
        abort_if(Gate::denies('countries_lang_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new CountriesLangResource($countriesLang->load(['language', 'country']));
    }

    public function update(UpdateCountriesLangRequest $request, CountriesLang $countriesLang)
    {
        $countriesLang->update($request->all());

        return (new CountriesLangResource($countriesLang))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(CountriesLang $countriesLang)
    {
        abort_if(Gate::denies('countries_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $countriesLang->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
