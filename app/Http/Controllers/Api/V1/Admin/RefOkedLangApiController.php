<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRefOkedLangRequest;
use App\Http\Requests\UpdateRefOkedLangRequest;
use App\Http\Resources\Admin\RefOkedLangResource;
use App\Models\RefOkedLang;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RefOkedLangApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ref_oked_lang_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

//        return new RefOkedLangResource(RefOkedLang::with(['language', 'oked'])->get());
        return new RefOkedLangResource(RefOkedLang::all());
    }

    public function store(StoreRefOkedLangRequest $request)
    {
        $refOkedLang = RefOkedLang::create($request->all());

        return (new RefOkedLangResource($refOkedLang))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(RefOkedLang $refOkedLang)
    {
        abort_if(Gate::denies('ref_oked_lang_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RefOkedLangResource($refOkedLang->load(['language', 'oked']));
    }

    public function update(UpdateRefOkedLangRequest $request, RefOkedLang $refOkedLang)
    {
        $refOkedLang->update($request->all());

        return (new RefOkedLangResource($refOkedLang))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(RefOkedLang $refOkedLang)
    {
        abort_if(Gate::denies('ref_oked_lang_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $refOkedLang->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
