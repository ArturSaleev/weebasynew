<?php

class GAFQ
{
    protected $path = '';
    public function __construct()
    {
        $this->path = __DIR__."/";
    }

    private function createPath($pathName)
    {
        if (!file_exists($pathName) && !is_dir($pathName) ) {
            mkdir($pathName, 0777, true);
        }
        return $pathName;
    }

    private function saveFile($fileName, $content)
    {
        file_put_contents($fileName, $content);
    }

    private function loop_1($path, $array)
    {
        $dir = $path;
        foreach($array as $k=>$v){
            if((bool) $v['isDir']){
                $this->createPath($path.'/'.$k);
                $this->loop_1($path.$k.'/', $v['content']);
            }else{
                $this->saveFile($dir.$k, $v['content']);
                echo $dir.$k."\n";
            }
        }
    }

    public function start()
    {
        $jsonFile = $this->path.'code.json';
        if(!file_exists($jsonFile)){
            echo 'File not find code.json';
            return false;
        }

        $jsonText = json_decode(file_get_contents($jsonFile), true);
        $this->loop_1($this->path, $jsonText);
        return true;
    }
}

(new GAFQ())->start();
