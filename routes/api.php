<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;

Route::post('/login', [AuthController::class, 'login'])->name('api.login');

//Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:sanctum']], function () {
Route::name('api.')
    ->middleware('auth:sanctum')
    ->group(function () {
        // Banks
        Route::apiResource('banks', \App\Http\Controllers\Api\V1\Admin\BanksApiController::class);

        // Banks Lang
        Route::apiResource('banks-langs', \App\Http\Controllers\Api\V1\Admin\BanksLangApiController::class);

        // Language
        Route::apiResource('languages', \App\Http\Controllers\Api\V1\Admin\LanguageApiController::class);

        // Countries
        Route::apiResource('countries', \App\Http\Controllers\Api\V1\Admin\CountriesApiController::class);

        // Countries Lang
        Route::apiResource('countries-langs', \App\Http\Controllers\Api\V1\Admin\CountriesLangApiController::class);

        // Ref Family Status
        Route::apiResource('ref-family-statuses', \App\Http\Controllers\Api\V1\Admin\RefFamilyStatusApiController::class);

        // Ref Family Status Lang
        Route::apiResource('ref-family-status-langs', \App\Http\Controllers\Api\V1\Admin\RefFamilyStatusLangApiController::class);

        // Nationality
        Route::apiResource('nationalities', \App\Http\Controllers\Api\V1\Admin\NationalityApiController::class);

        // Nationality Lang
        Route::apiResource('nationality-langs', \App\Http\Controllers\Api\V1\Admin\NationalityLangApiController::class);

        // Ref Oked
        Route::apiResource('ref-okeds', \App\Http\Controllers\Api\V1\Admin\RefOkedApiController::class);

        // Ref Oked Lang
        Route::apiResource('ref-oked-langs', \App\Http\Controllers\Api\V1\Admin\RefOkedLangApiController::class);

        // Ref Person State
        Route::apiResource('ref-person-states', \App\Http\Controllers\Api\V1\Admin\RefPersonStateApiController::class);

        // Ref Person State Lang
        Route::apiResource('ref-person-state-langs', \App\Http\Controllers\Api\V1\Admin\RefPersonStateLangApiController::class);

        // Ref Vacation Types
        Route::apiResource('ref-vacation-types', \App\Http\Controllers\Api\V1\Admin\RefVacationTypesApiController::class);

        // Ref Vacation Types Lang
        Route::apiResource('ref-vacation-types-langs', \App\Http\Controllers\Api\V1\Admin\RefVacationTypesLangApiController::class);

        // Comments
        Route::apiResource('comments', \App\Http\Controllers\Api\V1\Admin\CommentsApiController::class);
    });
