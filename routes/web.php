<?php

use App\Http\Controllers\HomeController;

Route::get('/', [HomeController::class, 'main']);
Route::get('/home', [HomeController::class, 'main']);
Route::get('/tags/{id}', [HomeController::class, 'tags']);
Route::get('/category/{id}', [HomeController::class, 'category']);
Route::get('/page/{id}', [HomeController::class, 'page']);
Route::post('/save_comment', [HomeController::class, 'setComment']);
//Route::get('/contacts', [HomeController::class, 'contacts']);


Auth::routes();

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth', 'admin']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Content Category
    Route::delete('content-categories/destroy', 'ContentCategoryController@massDestroy')->name('content-categories.massDestroy');
    Route::resource('content-categories', 'ContentCategoryController');

    // Content Tag
    Route::delete('content-tags/destroy', 'ContentTagController@massDestroy')->name('content-tags.massDestroy');
    Route::resource('content-tags', 'ContentTagController');

    // Content Page
    Route::delete('content-pages/destroy', 'ContentPageController@massDestroy')->name('content-pages.massDestroy');
    Route::post('content-pages/media', 'ContentPageController@storeMedia')->name('content-pages.storeMedia');
    Route::post('content-pages/ckmedia', 'ContentPageController@storeCKEditorImages')->name('content-pages.storeCKEditorImages');
    Route::resource('content-pages', 'ContentPageController');

    // Main Carousel
    Route::delete('main-carousels/destroy', 'MainCarouselController@massDestroy')->name('main-carousels.massDestroy');
    Route::post('main-carousels/media', 'MainCarouselController@storeMedia')->name('main-carousels.storeMedia');
    Route::post('main-carousels/ckmedia', 'MainCarouselController@storeCKEditorImages')->name('main-carousels.storeCKEditorImages');
    Route::resource('main-carousels', 'MainCarouselController');

    // Banks
    Route::delete('banks/destroy', 'BanksController@massDestroy')->name('banks.massDestroy');
    Route::resource('banks', 'BanksController');

    // Banks Lang
    Route::delete('banks-langs/destroy', 'BanksLangController@massDestroy')->name('banks-langs.massDestroy');
    Route::resource('banks-langs', 'BanksLangController');

    // Language
    Route::delete('languages/destroy', 'LanguageController@massDestroy')->name('languages.massDestroy');
    Route::resource('languages', 'LanguageController');

    // Countries
    Route::delete('countries/destroy', 'CountriesController@massDestroy')->name('countries.massDestroy');
    Route::resource('countries', 'CountriesController');

    // Countries Lang
    Route::delete('countries-langs/destroy', 'CountriesLangController@massDestroy')->name('countries-langs.massDestroy');
    Route::resource('countries-langs', 'CountriesLangController');

    // Ref Family Status
    Route::delete('ref-family-statuses/destroy', 'RefFamilyStatusController@massDestroy')->name('ref-family-statuses.massDestroy');
    Route::resource('ref-family-statuses', 'RefFamilyStatusController');

    // Ref Family Status Lang
    Route::delete('ref-family-status-langs/destroy', 'RefFamilyStatusLangController@massDestroy')->name('ref-family-status-langs.massDestroy');
    Route::resource('ref-family-status-langs', 'RefFamilyStatusLangController');

    // Nationality
    Route::delete('nationalities/destroy', 'NationalityController@massDestroy')->name('nationalities.massDestroy');
    Route::resource('nationalities', 'NationalityController');

    // Nationality Lang
    Route::delete('nationality-langs/destroy', 'NationalityLangController@massDestroy')->name('nationality-langs.massDestroy');
    Route::resource('nationality-langs', 'NationalityLangController');

    // Ref Oked
    Route::delete('ref-okeds/destroy', 'RefOkedController@massDestroy')->name('ref-okeds.massDestroy');
    Route::resource('ref-okeds', 'RefOkedController');

    // Ref Oked Lang
    Route::delete('ref-oked-langs/destroy', 'RefOkedLangController@massDestroy')->name('ref-oked-langs.massDestroy');
    Route::resource('ref-oked-langs', 'RefOkedLangController');

    // Ref Person State
    Route::delete('ref-person-states/destroy', 'RefPersonStateController@massDestroy')->name('ref-person-states.massDestroy');
    Route::resource('ref-person-states', 'RefPersonStateController');

    // Ref Person State Lang
    Route::delete('ref-person-state-langs/destroy', 'RefPersonStateLangController@massDestroy')->name('ref-person-state-langs.massDestroy');
    Route::resource('ref-person-state-langs', 'RefPersonStateLangController');

    // Ref Vacation Types
    Route::delete('ref-vacation-types/destroy', 'RefVacationTypesController@massDestroy')->name('ref-vacation-types.massDestroy');
    Route::resource('ref-vacation-types', 'RefVacationTypesController');

    // Ref Vacation Types Lang
    Route::delete('ref-vacation-types-langs/destroy', 'RefVacationTypesLangController@massDestroy')->name('ref-vacation-types-langs.massDestroy');
    Route::resource('ref-vacation-types-langs', 'RefVacationTypesLangController');

    // Comments
    Route::delete('comments/destroy', 'CommentsController@massDestroy')->name('comments.massDestroy');
    Route::resource('comments', 'CommentsController');
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
    // Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
        Route::post('profile', 'ChangePasswordController@updateProfile')->name('password.updateProfile');
        Route::post('profile/destroy', 'ChangePasswordController@destroy')->name('password.destroyProfile');
    }
});
Route::group(['as' => 'frontend.', 'namespace' => 'Frontend', 'middleware' => ['auth']], function () {
//    Route::get('/home', 'HomeController@index')->name('home');
//
//    // Permissions
//    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
//    Route::resource('permissions', 'PermissionsController');
//
//    // Roles
//    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
//    Route::resource('roles', 'RolesController');
//
//    // Users
//    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
//    Route::resource('users', 'UsersController');
//
//    // Content Category
//    Route::delete('content-categories/destroy', 'ContentCategoryController@massDestroy')->name('content-categories.massDestroy');
//    Route::resource('content-categories', 'ContentCategoryController');
//
//    // Content Tag
//    Route::delete('content-tags/destroy', 'ContentTagController@massDestroy')->name('content-tags.massDestroy');
//    Route::resource('content-tags', 'ContentTagController');
//
//    // Content Page
//    Route::delete('content-pages/destroy', 'ContentPageController@massDestroy')->name('content-pages.massDestroy');
//    Route::post('content-pages/media', 'ContentPageController@storeMedia')->name('content-pages.storeMedia');
//    Route::post('content-pages/ckmedia', 'ContentPageController@storeCKEditorImages')->name('content-pages.storeCKEditorImages');
//    Route::resource('content-pages', 'ContentPageController');
//
//    // Main Carousel
//    Route::delete('main-carousels/destroy', 'MainCarouselController@massDestroy')->name('main-carousels.massDestroy');
//    Route::post('main-carousels/media', 'MainCarouselController@storeMedia')->name('main-carousels.storeMedia');
//    Route::post('main-carousels/ckmedia', 'MainCarouselController@storeCKEditorImages')->name('main-carousels.storeCKEditorImages');
//    Route::resource('main-carousels', 'MainCarouselController');
//
//    // Banks
//    Route::delete('banks/destroy', 'BanksController@massDestroy')->name('banks.massDestroy');
//    Route::resource('banks', 'BanksController');
//
//    // Banks Lang
//    Route::delete('banks-langs/destroy', 'BanksLangController@massDestroy')->name('banks-langs.massDestroy');
//    Route::resource('banks-langs', 'BanksLangController');
//
//    // Language
//    Route::delete('languages/destroy', 'LanguageController@massDestroy')->name('languages.massDestroy');
//    Route::resource('languages', 'LanguageController');
//
//    // Countries
//    Route::delete('countries/destroy', 'CountriesController@massDestroy')->name('countries.massDestroy');
//    Route::resource('countries', 'CountriesController');
//
//    // Countries Lang
//    Route::delete('countries-langs/destroy', 'CountriesLangController@massDestroy')->name('countries-langs.massDestroy');
//    Route::resource('countries-langs', 'CountriesLangController');
//
//    // Ref Family Status
//    Route::delete('ref-family-statuses/destroy', 'RefFamilyStatusController@massDestroy')->name('ref-family-statuses.massDestroy');
//    Route::resource('ref-family-statuses', 'RefFamilyStatusController');
//
//    // Ref Family Status Lang
//    Route::delete('ref-family-status-langs/destroy', 'RefFamilyStatusLangController@massDestroy')->name('ref-family-status-langs.massDestroy');
//    Route::resource('ref-family-status-langs', 'RefFamilyStatusLangController');
//
//    // Nationality
//    Route::delete('nationalities/destroy', 'NationalityController@massDestroy')->name('nationalities.massDestroy');
//    Route::resource('nationalities', 'NationalityController');
//
//    // Nationality Lang
//    Route::delete('nationality-langs/destroy', 'NationalityLangController@massDestroy')->name('nationality-langs.massDestroy');
//    Route::resource('nationality-langs', 'NationalityLangController');
//
//    // Ref Oked
//    Route::delete('ref-okeds/destroy', 'RefOkedController@massDestroy')->name('ref-okeds.massDestroy');
//    Route::resource('ref-okeds', 'RefOkedController');
//
//    // Ref Oked Lang
//    Route::delete('ref-oked-langs/destroy', 'RefOkedLangController@massDestroy')->name('ref-oked-langs.massDestroy');
//    Route::resource('ref-oked-langs', 'RefOkedLangController');
//
//    // Ref Person State
//    Route::delete('ref-person-states/destroy', 'RefPersonStateController@massDestroy')->name('ref-person-states.massDestroy');
//    Route::resource('ref-person-states', 'RefPersonStateController');
//
//    // Ref Person State Lang
//    Route::delete('ref-person-state-langs/destroy', 'RefPersonStateLangController@massDestroy')->name('ref-person-state-langs.massDestroy');
//    Route::resource('ref-person-state-langs', 'RefPersonStateLangController');
//
//    // Ref Vacation Types
//    Route::delete('ref-vacation-types/destroy', 'RefVacationTypesController@massDestroy')->name('ref-vacation-types.massDestroy');
//    Route::resource('ref-vacation-types', 'RefVacationTypesController');
//
//    // Ref Vacation Types Lang
//    Route::delete('ref-vacation-types-langs/destroy', 'RefVacationTypesLangController@massDestroy')->name('ref-vacation-types-langs.massDestroy');
//    Route::resource('ref-vacation-types-langs', 'RefVacationTypesLangController');
//
//    // Comments
//    Route::delete('comments/destroy', 'CommentsController@massDestroy')->name('comments.massDestroy');
//    Route::resource('comments', 'CommentsController');

    Route::get('frontend/profile', 'ProfileController@index')->name('profile.index');
    Route::post('frontend/profile', 'ProfileController@update')->name('profile.update');
    Route::post('frontend/profile/destroy', 'ProfileController@destroy')->name('profile.destroy');
    Route::post('frontend/profile/password', 'ProfileController@password')->name('profile.password');
});
