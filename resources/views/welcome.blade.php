@extends('layouts.main')
@section('content')
    <div class="col-12 col-lg-12 col-md-12">
    @foreach($result as $item)
        <div class="card mb-3">
            <div class="card-body">
                <a href="{{ asset('/page/'.$item->id) }}">
                    <h3>{{ $item->title }}</h3>
                    <div>{!! $item->excerpt !!}</div>
                </a>
            </div>
            <div class="card-footer">
                @foreach($item->categories as $category)
                    <a href="{{ asset('/category/'.$category->id) }}" class="badge super-badge bg-tertiary">
                        {{ $category->name }}
                    </a>
                @endforeach

                @foreach($item->tags as $tags)
                    <a href="{{ asset('/tags/'.$tags->id) }}" class="badge super-badge bg-info">
                        {{ $tags->name }}
                    </a>
                @endforeach
            </div>
        </div>
    @endforeach
    </div>
    <div class="col-lg-12">
        {{ $result->links() }}
    </div>
@endsection
