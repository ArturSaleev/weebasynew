<div id="sidebar" class="c-sidebar c-sidebar-fixed c-sidebar-lg-show">

    <div class="c-sidebar-brand d-md-down-none">
        <a class="c-sidebar-brand-full h4" href="#">
            {{ trans('panel.site_title') }}
        </a>
    </div>

    <ul class="c-sidebar-nav">
        <li class="c-sidebar-nav-item">
            <a href="{{ route("admin.home") }}" class="c-sidebar-nav-link">
                <i class="c-sidebar-nav-icon fas fa-fw fa-tachometer-alt">

                </i>
                {{ trans('global.dashboard') }}
            </a>
        </li>
        @can('user_management_access')
            <li class="c-sidebar-nav-dropdown {{ request()->is("admin/permissions*") ? "c-show" : "" }} {{ request()->is("admin/roles*") ? "c-show" : "" }} {{ request()->is("admin/users*") ? "c-show" : "" }}">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-users c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.userManagement.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('permission_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.permissions.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/permissions") || request()->is("admin/permissions/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-unlock-alt c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.permission.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('role_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.roles.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/roles") || request()->is("admin/roles/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-briefcase c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.role.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('user_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.users.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/users") || request()->is("admin/users/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-user c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.user.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('content_management_access')
            <li class="c-sidebar-nav-dropdown {{ request()->is("admin/content-categories*") ? "c-show" : "" }} {{ request()->is("admin/content-tags*") ? "c-show" : "" }} {{ request()->is("admin/content-pages*") ? "c-show" : "" }} {{ request()->is("admin/main-carousels*") ? "c-show" : "" }} {{ request()->is("admin/comments*") ? "c-show" : "" }}">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-book c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.contentManagement.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('content_category_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.content-categories.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/content-categories") || request()->is("admin/content-categories/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-folder c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.contentCategory.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('content_tag_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.content-tags.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/content-tags") || request()->is("admin/content-tags/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-tags c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.contentTag.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('content_page_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.content-pages.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/content-pages") || request()->is("admin/content-pages/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-file c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.contentPage.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('main_carousel_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.main-carousels.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/main-carousels") || request()->is("admin/main-carousels/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-cogs c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.mainCarousel.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('comment_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.comments.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/comments") || request()->is("admin/comments/*") ? "c-active" : "" }}">
                                <i class="fa-fw far fa-comments c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.comment.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('api_access')
            <li class="c-sidebar-nav-dropdown {{ request()->is("admin/banks*") ? "c-show" : "" }} {{ request()->is("admin/banks-langs*") ? "c-show" : "" }} {{ request()->is("admin/languages*") ? "c-show" : "" }} {{ request()->is("admin/countries*") ? "c-show" : "" }} {{ request()->is("admin/countries-langs*") ? "c-show" : "" }} {{ request()->is("admin/ref-family-statuses*") ? "c-show" : "" }} {{ request()->is("admin/ref-family-status-langs*") ? "c-show" : "" }} {{ request()->is("admin/nationalities*") ? "c-show" : "" }} {{ request()->is("admin/nationality-langs*") ? "c-show" : "" }} {{ request()->is("admin/ref-okeds*") ? "c-show" : "" }} {{ request()->is("admin/ref-oked-langs*") ? "c-show" : "" }} {{ request()->is("admin/ref-person-states*") ? "c-show" : "" }} {{ request()->is("admin/ref-person-state-langs*") ? "c-show" : "" }} {{ request()->is("admin/ref-vacation-types*") ? "c-show" : "" }} {{ request()->is("admin/ref-vacation-types-langs*") ? "c-show" : "" }}">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-passport c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.api.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('language_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.languages.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/languages") || request()->is("admin/languages/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-language c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.language.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('bank_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.banks.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/banks") || request()->is("admin/banks/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-university c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.bank.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('banks_lang_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.banks-langs.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/banks-langs") || request()->is("admin/banks-langs/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-university c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.banksLang.title') }}
                            </a>
                        </li>
                    @endcan

                    @can('country_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.countries.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/countries") || request()->is("admin/countries/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-flag c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.country.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('countries_lang_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.countries-langs.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/countries-langs") || request()->is("admin/countries-langs/*") ? "c-active" : "" }}">
                                <i class="fa-fw fab fa-font-awesome-flag c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.countriesLang.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('ref_family_status_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.ref-family-statuses.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/ref-family-statuses") || request()->is("admin/ref-family-statuses/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-user-tag c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.refFamilyStatus.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('ref_family_status_lang_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.ref-family-status-langs.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/ref-family-status-langs") || request()->is("admin/ref-family-status-langs/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-user-tag c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.refFamilyStatusLang.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('nationality_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.nationalities.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/nationalities") || request()->is("admin/nationalities/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-globe c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.nationality.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('nationality_lang_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.nationality-langs.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/nationality-langs") || request()->is("admin/nationality-langs/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-globe c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.nationalityLang.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('ref_oked_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.ref-okeds.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/ref-okeds") || request()->is("admin/ref-okeds/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-puzzle-piece c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.refOked.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('ref_oked_lang_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.ref-oked-langs.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/ref-oked-langs") || request()->is("admin/ref-oked-langs/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-puzzle-piece c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.refOkedLang.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('ref_person_state_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.ref-person-states.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/ref-person-states") || request()->is("admin/ref-person-states/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-user-clock c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.refPersonState.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('ref_person_state_lang_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.ref-person-state-langs.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/ref-person-state-langs") || request()->is("admin/ref-person-state-langs/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-user-clock c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.refPersonStateLang.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('ref_vacation_type_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.ref-vacation-types.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/ref-vacation-types") || request()->is("admin/ref-vacation-types/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-car c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.refVacationType.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('ref_vacation_types_lang_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.ref-vacation-types-langs.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/ref-vacation-types-langs") || request()->is("admin/ref-vacation-types-langs/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-car c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.refVacationTypesLang.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @if(file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php')))
            @can('profile_password_edit')
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link {{ request()->is('profile/password') || request()->is('profile/password/*') ? 'c-active' : '' }}" href="{{ route('profile.password.edit') }}">
                        <i class="fa-fw fas fa-key c-sidebar-nav-icon">
                        </i>
                        {{ trans('global.change_password') }}
                    </a>
                </li>
            @endcan
        @endif
        <li class="c-sidebar-nav-item">
            <a href="#" class="c-sidebar-nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                <i class="c-sidebar-nav-icon fas fa-fw fa-sign-out-alt">

                </i>
                {{ trans('global.logout') }}
            </a>
        </li>
    </ul>

</div>
