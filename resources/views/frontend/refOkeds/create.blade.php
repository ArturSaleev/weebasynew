@extends('layouts.frontend')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    {{ trans('global.create') }} {{ trans('cruds.refOked.title_singular') }}
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route("frontend.ref-okeds.store") }}" enctype="multipart/form-data">
                        @method('POST')
                        @csrf
                        <div class="form-group">
                            <label for="oked">{{ trans('cruds.refOked.fields.oked') }}</label>
                            <input class="form-control" type="text" name="oked" id="oked" value="{{ old('oked', '') }}">
                            @if($errors->has('oked'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('oked') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.refOked.fields.oked_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="name_oked">{{ trans('cruds.refOked.fields.name_oked') }}</label>
                            <input class="form-control" type="text" name="name_oked" id="name_oked" value="{{ old('name_oked', '') }}">
                            @if($errors->has('name_oked'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('name_oked') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.refOked.fields.name_oked_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="name">{{ trans('cruds.refOked.fields.name') }}</label>
                            <input class="form-control" type="text" name="name" id="name" value="{{ old('name', '') }}">
                            @if($errors->has('name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('name') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.refOked.fields.name_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection