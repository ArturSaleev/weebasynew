@extends('layouts.frontend')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    {{ trans('global.show') }} {{ trans('cruds.refOked.title') }}
                </div>

                <div class="card-body">
                    <div class="form-group">
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('frontend.ref-okeds.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>
                                        {{ trans('cruds.refOked.fields.id') }}
                                    </th>
                                    <td>
                                        {{ $refOked->id }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.refOked.fields.oked') }}
                                    </th>
                                    <td>
                                        {{ $refOked->oked }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.refOked.fields.name_oked') }}
                                    </th>
                                    <td>
                                        {{ $refOked->name_oked }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.refOked.fields.name') }}
                                    </th>
                                    <td>
                                        {{ $refOked->name }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('frontend.ref-okeds.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection