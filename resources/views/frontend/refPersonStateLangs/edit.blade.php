@extends('layouts.frontend')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    {{ trans('global.edit') }} {{ trans('cruds.refPersonStateLang.title_singular') }}
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route("frontend.ref-person-state-langs.update", [$refPersonStateLang->id]) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="language_id">{{ trans('cruds.refPersonStateLang.fields.language') }}</label>
                            <select class="form-control select2" name="language_id" id="language_id">
                                @foreach($languages as $id => $entry)
                                    <option value="{{ $id }}" {{ (old('language_id') ? old('language_id') : $refPersonStateLang->language->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('language'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('language') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.refPersonStateLang.fields.language_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="person_state_id">{{ trans('cruds.refPersonStateLang.fields.person_state') }}</label>
                            <select class="form-control select2" name="person_state_id" id="person_state_id">
                                @foreach($person_states as $id => $entry)
                                    <option value="{{ $id }}" {{ (old('person_state_id') ? old('person_state_id') : $refPersonStateLang->person_state->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('person_state'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('person_state') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.refPersonStateLang.fields.person_state_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="name">{{ trans('cruds.refPersonStateLang.fields.name') }}</label>
                            <input class="form-control" type="text" name="name" id="name" value="{{ old('name', $refPersonStateLang->name) }}">
                            @if($errors->has('name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('name') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.refPersonStateLang.fields.name_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection