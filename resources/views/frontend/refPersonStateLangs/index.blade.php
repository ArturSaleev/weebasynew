@extends('layouts.frontend')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @can('ref_person_state_lang_create')
                <div style="margin-bottom: 10px;" class="row">
                    <div class="col-lg-12">
                        <a class="btn btn-success" href="{{ route('frontend.ref-person-state-langs.create') }}">
                            {{ trans('global.add') }} {{ trans('cruds.refPersonStateLang.title_singular') }}
                        </a>
                    </div>
                </div>
            @endcan
            <div class="card">
                <div class="card-header">
                    {{ trans('cruds.refPersonStateLang.title_singular') }} {{ trans('global.list') }}
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable datatable-RefPersonStateLang">
                            <thead>
                                <tr>
                                    <th>
                                        {{ trans('cruds.refPersonStateLang.fields.id') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.refPersonStateLang.fields.language') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.refPersonStateLang.fields.person_state') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.refPersonStateLang.fields.name') }}
                                    </th>
                                    <th>
                                        &nbsp;
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($refPersonStateLangs as $key => $refPersonStateLang)
                                    <tr data-entry-id="{{ $refPersonStateLang->id }}">
                                        <td>
                                            {{ $refPersonStateLang->id ?? '' }}
                                        </td>
                                        <td>
                                            {{ $refPersonStateLang->language->name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $refPersonStateLang->person_state->name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $refPersonStateLang->name ?? '' }}
                                        </td>
                                        <td>
                                            @can('ref_person_state_lang_show')
                                                <a class="btn btn-xs btn-primary" href="{{ route('frontend.ref-person-state-langs.show', $refPersonStateLang->id) }}">
                                                    {{ trans('global.view') }}
                                                </a>
                                            @endcan

                                            @can('ref_person_state_lang_edit')
                                                <a class="btn btn-xs btn-info" href="{{ route('frontend.ref-person-state-langs.edit', $refPersonStateLang->id) }}">
                                                    {{ trans('global.edit') }}
                                                </a>
                                            @endcan

                                            @can('ref_person_state_lang_delete')
                                                <form action="{{ route('frontend.ref-person-state-langs.destroy', $refPersonStateLang->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                                </form>
                                            @endcan

                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('ref_person_state_lang_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('frontend.ref-person-state-langs.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-RefPersonStateLang:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection