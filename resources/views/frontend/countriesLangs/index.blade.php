@extends('layouts.frontend')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @can('countries_lang_create')
                <div style="margin-bottom: 10px;" class="row">
                    <div class="col-lg-12">
                        <a class="btn btn-success" href="{{ route('frontend.countries-langs.create') }}">
                            {{ trans('global.add') }} {{ trans('cruds.countriesLang.title_singular') }}
                        </a>
                    </div>
                </div>
            @endcan
            <div class="card">
                <div class="card-header">
                    {{ trans('cruds.countriesLang.title_singular') }} {{ trans('global.list') }}
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable datatable-CountriesLang">
                            <thead>
                                <tr>
                                    <th>
                                        {{ trans('cruds.countriesLang.fields.id') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.countriesLang.fields.language') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.countriesLang.fields.country') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.countriesLang.fields.name') }}
                                    </th>
                                    <th>
                                        &nbsp;
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($countriesLangs as $key => $countriesLang)
                                    <tr data-entry-id="{{ $countriesLang->id }}">
                                        <td>
                                            {{ $countriesLang->id ?? '' }}
                                        </td>
                                        <td>
                                            {{ $countriesLang->language->name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $countriesLang->country->name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $countriesLang->name ?? '' }}
                                        </td>
                                        <td>
                                            @can('countries_lang_show')
                                                <a class="btn btn-xs btn-primary" href="{{ route('frontend.countries-langs.show', $countriesLang->id) }}">
                                                    {{ trans('global.view') }}
                                                </a>
                                            @endcan

                                            @can('countries_lang_edit')
                                                <a class="btn btn-xs btn-info" href="{{ route('frontend.countries-langs.edit', $countriesLang->id) }}">
                                                    {{ trans('global.edit') }}
                                                </a>
                                            @endcan

                                            @can('countries_lang_delete')
                                                <form action="{{ route('frontend.countries-langs.destroy', $countriesLang->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                                </form>
                                            @endcan

                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('countries_lang_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('frontend.countries-langs.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-CountriesLang:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection