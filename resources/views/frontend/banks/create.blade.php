@extends('layouts.frontend')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    {{ trans('global.create') }} {{ trans('cruds.bank.title_singular') }}
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route("frontend.banks.store") }}" enctype="multipart/form-data">
                        @method('POST')
                        @csrf
                        <div class="form-group">
                            <label for="name">{{ trans('cruds.bank.fields.name') }}</label>
                            <input class="form-control" type="text" name="name" id="name" value="{{ old('name', '') }}">
                            @if($errors->has('name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('name') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.bank.fields.name_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="short_name">{{ trans('cruds.bank.fields.short_name') }}</label>
                            <input class="form-control" type="text" name="short_name" id="short_name" value="{{ old('short_name', '') }}">
                            @if($errors->has('short_name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('short_name') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.bank.fields.short_name_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="logo">{{ trans('cruds.bank.fields.logo') }}</label>
                            <input class="form-control" type="text" name="logo" id="logo" value="{{ old('logo', '') }}">
                            @if($errors->has('logo'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('logo') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.bank.fields.logo_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="mfo">{{ trans('cruds.bank.fields.mfo') }}</label>
                            <input class="form-control" type="text" name="mfo" id="mfo" value="{{ old('mfo', '') }}">
                            @if($errors->has('mfo'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('mfo') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.bank.fields.mfo_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="mfo_head">{{ trans('cruds.bank.fields.mfo_head') }}</label>
                            <input class="form-control" type="text" name="mfo_head" id="mfo_head" value="{{ old('mfo_head', '') }}">
                            @if($errors->has('mfo_head'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('mfo_head') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.bank.fields.mfo_head_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="mfo_rkc">{{ trans('cruds.bank.fields.mfo_rkc') }}</label>
                            <input class="form-control" type="text" name="mfo_rkc" id="mfo_rkc" value="{{ old('mfo_rkc', '') }}">
                            @if($errors->has('mfo_rkc'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('mfo_rkc') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.bank.fields.mfo_rkc_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="kor_account">{{ trans('cruds.bank.fields.kor_account') }}</label>
                            <input class="form-control" type="text" name="kor_account" id="kor_account" value="{{ old('kor_account', '') }}">
                            @if($errors->has('kor_account'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('kor_account') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.bank.fields.kor_account_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <div>
                                <input type="hidden" name="status" value="0">
                                <input type="checkbox" name="status" id="status" value="1" {{ old('status', 0) == 1 || old('status') === null ? 'checked' : '' }}>
                                <label for="status">{{ trans('cruds.bank.fields.status') }}</label>
                            </div>
                            @if($errors->has('status'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('status') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.bank.fields.status_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="bin">{{ trans('cruds.bank.fields.bin') }}</label>
                            <input class="form-control" type="text" name="bin" id="bin" value="{{ old('bin', '') }}">
                            @if($errors->has('bin'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('bin') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.bank.fields.bin_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="bik">{{ trans('cruds.bank.fields.bik') }}</label>
                            <input class="form-control" type="text" name="bik" id="bik" value="{{ old('bik', '') }}">
                            @if($errors->has('bik'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('bik') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.bank.fields.bik_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="commis">{{ trans('cruds.bank.fields.commis') }}</label>
                            <input class="form-control" type="number" name="commis" id="commis" value="{{ old('commis', '0') }}" step="0.0001">
                            @if($errors->has('commis'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('commis') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.bank.fields.commis_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection