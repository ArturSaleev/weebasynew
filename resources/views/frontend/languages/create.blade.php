@extends('layouts.frontend')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    {{ trans('global.create') }} {{ trans('cruds.language.title_singular') }}
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route("frontend.languages.store") }}" enctype="multipart/form-data">
                        @method('POST')
                        @csrf
                        <div class="form-group">
                            <label for="name">{{ trans('cruds.language.fields.name') }}</label>
                            <input class="form-control" type="text" name="name" id="name" value="{{ old('name', '') }}">
                            @if($errors->has('name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('name') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.language.fields.name_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="short">{{ trans('cruds.language.fields.short') }}</label>
                            <input class="form-control" type="text" name="short" id="short" value="{{ old('short', '') }}">
                            @if($errors->has('short'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('short') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.language.fields.short_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="logo">{{ trans('cruds.language.fields.logo') }}</label>
                            <input class="form-control" type="text" name="logo" id="logo" value="{{ old('logo', '') }}">
                            @if($errors->has('logo'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('logo') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.language.fields.logo_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection