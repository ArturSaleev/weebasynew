<?php
    $categories = \App\Helpers\MainHelper::MenuCategoryes();
    $tags = \App\Helpers\MainHelper::MenuTags();
?>
<nav id="sidebarMenu"
     class="sidebar d-lg-block bg-gray-800 text-white collapse simplebar-scrollable-y"
     data-simplebar="init"
     style=""
>
    <div class="simplebar-wrapper" style="margin: 0px;">
        <div class="simplebar-height-auto-observer-wrapper">
            <div class="simplebar-height-auto-observer"></div>
        </div>
        <div class="simplebar-mask">
            <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
                <div class="simplebar-content-wrapper" style="height: auto; overflow: hidden scroll;">
                    <div class="simplebar-content" style="padding: 0px;">
                        <div class="sidebar-inner px-4 pt-3">
                            <div
                                class="user-card d-flex d-md-none align-items-center justify-content-between justify-content-md-center pb-4">

                                <div class="collapse-close d-md-none">
                                    <a href="#sidebarMenu"
                                       data-bs-toggle="collapse"
                                       data-bs-target="#sidebarMenu"
                                       aria-controls="sidebarMenu"
                                       aria-expanded="false"
                                       aria-label="Toggle navigation"
                                       class="collapsed"
                                    >
                                        <svg class="icon icon-xs" fill="currentColor" viewBox="0 0 20 20"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                                  clip-rule="evenodd"></path>
                                        </svg>
                                    </a>
                                </div>
                            </div>

                            <ul class="nav flex-column pt-3 pt-md-0">
                                <li class="nav-item">
                                    <a href="{{ asset('/') }}" class="nav-link d-flex align-items-center">
                                        <span class="sidebar-icon">
                                            <img src="{{ asset('images/logo.png') }}" height="40" width="50">
                                        </span>
                                        <span class="mt-1 ms-1 sidebar-text h2">WebEasy</span>
                                    </a>
                                </li>
                                <li class="nav-item active">
                                    <a href="{{ asset('/') }}" class="nav-link">
                                        <span class="sidebar-icon">
                                            <i class="fa fa-home"></i>
                                        </span>
                                        <span class="sidebar-text">Главная</span>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <span class="nav-link collapsed d-flex justify-content-between align-items-center"
                                          data-bs-toggle="collapse"
                                          data-bs-target="#submenu-app"
                                    >
                                        <span>
                                            <span class="sidebar-icon">
                                                <i class="fa fa-book"></i>
                                            </span>
                                            <span class="sidebar-text">Категории</span>
                                        </span>
                                        <span class="link-arrow">
                                            <svg class="icon icon-sm" fill="currentColor" viewBox="0 0 20 20"
                                                 xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd"
                                                                                          d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                                                                          clip-rule="evenodd"></path></svg>
                                        </span>
                                    </span>

                                    <div class="multi-level collapse" role="list" id="submenu-app"
                                         aria-expanded="false">
                                        <ul class="flex-column nav">
                                            @foreach($categories as $category)
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ asset('category/'.$category->id) }}">
                                                        @if($category->slug !== '')
                                                        <span class="sidebar-icon">
                                                            <i class="{{ $category->slug }}"></i>
                                                        </span>
                                                        @endif
                                                        <span class="sidebar-text">{{ $category->name }}</span>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </li>


                                <li class="nav-item">
                                    <span class="nav-link collapsed d-flex justify-content-between align-items-center"
                                          data-bs-toggle="collapse" data-bs-target="#submenu-pages">
                                        <span>
                                            <span class="sidebar-icon">
                                                <i class="fa fa-tags"></i>
                                            </span>
                                            <span class="sidebar-text">Теги</span>
                                        </span>
                                        <span class="link-arrow">
                                            <svg class="icon icon-sm" fill="currentColor" viewBox="0 0 20 20"
                                                 xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd"
                                                                                          d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                                                                          clip-rule="evenodd"></path></svg>
                                        </span>
                                    </span>
                                    <div class="multi-level collapse" role="list" id="submenu-pages"
                                         aria-expanded="false">
                                        <ul class="flex-column nav">
                                            @foreach($tags as $tag)
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ asset('tags/'.$tag->id) }}">
                                                        <span class="sidebar-text">{{ $tag->name }}</span>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </li>
                                <li role="separator" class="dropdown-divider mt-4 mb-3 border-gray-700"></li>

{{--                                <li class="nav-item">--}}
{{--                                    <a href="{{ asset('contacts') }}" target="_blank"--}}
{{--                                       class="nav-link d-flex align-items-center">--}}
{{--                                        <span class="sidebar-icon">--}}
{{--                                            <i class="fa fa-map-marked"></i>--}}
{{--                                        </span>--}}
{{--                                        <span class="sidebar-text">Контакты</span>--}}
{{--                                    </a>--}}
{{--                                </li>--}}

                                @auth
                                    <li class="nav-item">
                                        <a href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();"
                                           class="nav-link d-flex align-items-center">
                                        <span class="sidebar-icon">
                                            <i class="fa fa-power-off"></i>
                                        </span>
                                            <span class="sidebar-text">Выйти</span>
                                        </a>
                                    </li>
                                    <form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                @endauth
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="simplebar-placeholder" style="width: 260px; height: 621px;"></div>
    </div>
    <div class="simplebar-track simplebar-horizontal" style="visibility: hidden;">
        <div class="simplebar-scrollbar" style="width: 0px; display: none;"></div>
    </div>
    <div class="simplebar-track simplebar-vertical" style="visibility: visible;">
        <div class="simplebar-scrollbar"
             style="height: 288px; transform: translate3d(0px, 0px, 0px); display: block;"></div>
    </div>
</nav>
