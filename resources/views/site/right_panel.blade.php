<?php
$url = url()->current();
$array = explode('/', $url);
$onPage = in_array('page', $array);
if ($onPage) {
    $domain = Request::root();
    $activePage = str_replace($domain . '/page/', '', $url);
}
?>
<div class="card">
    @auth
        <div class="card">
            @if($onPage)
                <div class="card-header">
                    Комментарии
                </div>
                <div class="card-body" style="height: 60vh; max-height: 60%">
                    @if($result->pageComments)
                        @foreach($result->pageComments as $comment)
                            <div class="card bg-gray-800 text-white border-0 shadow p-4 ms-md-5 ms-lg-6 mb-4">
                                <div class="d-flex justify-content-between align-items-center mb-2">
                                <span class="font-small">
                                    <span class="fw-bold">{{$comment->user->name}}</span>
                                    <span class="text-sm-end font-small">{{ date('d.m.Y H:i', strtotime($comment->created_at)) }}</span>
                                </span>
                                </div>
                                <p class="text-gray-300 m-0">
                                    {{ $comment->comment }}
                                </p>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="card-footer">
                    <form method="post" action="{{ asset('save_comment') }}">
                        @csrf
                        <textarea name="body" class="form-control" maxlength="150" cols="5"></textarea>
                        <input type="hidden" name="page_id" value="{{ $activePage }}">
                        <br/>
                        <button class="btn btn-block btn-primary">Отправить</button>
                    </form>
                </div>

            @else
                <div class="card-body">
                    <img src="{{ asset('images/logo.png') }}" style="width: 100%"/>
                </div>
            @endif
        </div>
    @else
        <div class="card p-4">
            <a href="{{ asset('/login') }}" class="btn btn-outline-indigo btn-block mb-4">
                {{ trans('global.login') }}
            </a>
            <a href="{{ asset('/register') }}"
               class="btn btn-outline-indigo btn-block">{{ trans('global.register') }}</a>
        </div>
    @endauth
</div>
