<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="../../assets/img/favicon/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="../../assets/img/favicon/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="../../assets/img/favicon/favicon-16x16.png" sizes="16x16" type="image/png">

    <link rel="mask-icon" href="../../assets/img/favicon/safari-pinned-tab.svg" color="#563d7c">
    <link rel="icon" href="../../assets/img/favicon/favicon.ico">
    <meta name="msapplication-config" content="../../assets/img/favicons/browserconfig.xml">
    <meta name="theme-color" content="#563d7c">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <!-- Datepicker -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/vanillajs-datepicker@1.1.4/dist/css/datepicker.min.css">
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/vanillajs-datepicker@1.1.4/dist/css/datepicker-bs4.min.css">

    <!-- Fontawesome -->
    <link type="text/css" href="/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">

    <!-- Sweet Alert -->
    <link type="text/css" href="/vendor/sweetalert2/sweetalert2.min.css" rel="stylesheet">

    <!-- Notyf -->
    <link type="text/css" href="/vendor/notyf/notyf.min.css" rel="stylesheet">

    <!-- Volt CSS -->
    <link type="text/css" href="/css/volt.css" rel="stylesheet">

    <!-- Core -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
            integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js"
            integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT"
            crossorigin="anonymous"></script>

    <!-- Vendor JS -->
    <script src="/js/on-screen.umd.min.js"></script>

    <!-- Slider -->
    <script src="/js/nouislider.min.js"></script>

    <!-- Smooth scroll -->
    <script src="/js/smooth-scroll.polyfills.min.js"></script>

    <!-- Apex Charts -->
    <script src="/vendor/apexcharts/apexcharts.min.js"></script>

    <!-- Charts -->
    <script src="/js/chartist.min.js"></script>
    <script src="/js/chartist-plugin-tooltip.min.js"></script>

    <!-- Datepicker -->
    <script src="https://cdn.jsdelivr.net/npm/vanillajs-datepicker@1.1.4/dist/js/datepicker.min.js"></script>

    <!-- Sweet Alerts 2 -->
    <script src="/js/sweetalert2.all.min.js"></script>

    <!-- Moment JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>

    <!-- Notyf -->
    <script src="/vendor/notyf/notyf.min.js"></script>

    <!-- Simplebar -->
    <script src="/js/simplebar.min.js"></script>

    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>

    <!-- Volt JS -->
    <script src="/js/volt.js"></script>
    @yield('styles')
</head>

<body>
@include('site.navbar_mobile')
@include('site.navbar')

<main class="content">
    @include('site.header')

    <div class="py-4">
        <div class="row">
            <div class="col-lg-8">
                @yield('content')
            </div>
            <div class="col-lg-4">
                @include('site.right_panel')
            </div>
        </div>
    </div>

{{--    @include('site.footer')--}}
</main>


<script src="/vendor/@popperjs/core/dist/umd/popper.min.js"></script>
<script src="/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/vendor/onscreen/dist/on-screen.umd.min.js"></script>
<script src="/vendor/nouislider/distribute/nouislider.min.js"></script>
<script src="/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
<script src="/vendor/chartist/dist/chartist.min.js"></script>
<script src="/vendor/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
<script src="/vendor/vanillajs-datepicker/dist/js/datepicker.min.js"></script>
<script src="/vendor/sweetalert2/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>
<script src="/vendor/vanillajs-datepicker/dist/js/datepicker.min.js"></script>
<script src="/vendor/notyf/notyf.min.js"></script>
<script src="/vendor/simplebar/dist/simplebar.min.js"></script>
<script async="" defer="defer" src="https://buttons.github.io/buttons.js"></script>
<script src="../assets/js/volt.js"></script>
<script defer=""
        src="https://static.cloudflareinsights.com/beacon.min.js/v52afc6f149f6479b8c77fa569edb01181681764108816"
        integrity="sha512-jGCTpDpBAYDGNYR5ztKt4BQPGef1P0giN6ZGVUi835kFF88FOmmn8jBQWNgrNd8g/Yu421NdgWhwQoaOPFflDw=="
        data-cf-beacon="{&quot;rayId&quot;:&quot;7bd04c74acd9bba1&quot;,&quot;version&quot;:&quot;2023.3.0&quot;,&quot;r&quot;:1,&quot;token&quot;:&quot;3a2c60bab7654724a0f7e5946db4ea5a&quot;,&quot;si&quot;:100}"
        crossorigin="anonymous"></script>
<script type="text/javascript" id="" src="https://cdn.paddle.com/paddle/paddle.js"></script>
<script type="text/javascript" id="">if ("demo.themesberg.com" === window.location.hostname) {
        Paddle.Setup({vendor: 113942});
        var setDomain = "themesberg.com", getCookies = function () {
                for (var b = document.cookie.split(";"), c = {}, a = 0; a < b.length; a++) {
                    var d = b[a].split("\x3d");
                    c[(d[0] + "").trim()] = decodeURIComponent(d[1])
                }
                return c
            }, setCookie = function (b, c, a, d) {
                var e = new Date;
                e.setTime(e.getTime() + 864E5 * a);
                a = "expires\x3d" + e.toUTCString();
                document.cookie = b + "\x3d" + c + ";" + a + ";domain\x3d" + d + ";path\x3d/"
            }, deleteCookie = function (b) {
                document.cookie = b + "\x3d;expires\x3dThu, 01 Jan 1970 00:00:01 GMT;"
            },
            setPaddleCookies = function (b) {
                var c = getCookies();
                Object.keys(c).forEach(function (a) {
                    a.startsWith("paddlejs_") && (deleteCookie(a), setCookie(a, c[a], 30, b))
                })
            };
        setPaddleCookies(setDomain)
    }
    ;</script>


@yield('scripts')
</body>

</html>
