@extends('layouts.main')
@section('content')
    <div class="card">
    <div class="card-body text-justify">
        <h3>{{ $result->title }}</h3>
        <hr />
        {!! $result->page_text !!}
    </div>
    </div>
@endsection
