@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.language.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.languages.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.language.fields.id') }}
                        </th>
                        <td>
                            {{ $language->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.language.fields.name') }}
                        </th>
                        <td>
                            {{ $language->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.language.fields.short') }}
                        </th>
                        <td>
                            {{ $language->short }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.language.fields.logo') }}
                        </th>
                        <td>
                            {{ $language->logo }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.languages.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item">
            <a class="nav-link" href="#language_banks_langs" role="tab" data-toggle="tab">
                {{ trans('cruds.banksLang.title') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#language_countries_langs" role="tab" data-toggle="tab">
                {{ trans('cruds.countriesLang.title') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#language_ref_family_status_langs" role="tab" data-toggle="tab">
                {{ trans('cruds.refFamilyStatusLang.title') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#language_nationality_langs" role="tab" data-toggle="tab">
                {{ trans('cruds.nationalityLang.title') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#language_ref_oked_langs" role="tab" data-toggle="tab">
                {{ trans('cruds.refOkedLang.title') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#language_ref_person_state_langs" role="tab" data-toggle="tab">
                {{ trans('cruds.refPersonStateLang.title') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#language_ref_vacation_types_langs" role="tab" data-toggle="tab">
                {{ trans('cruds.refVacationTypesLang.title') }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" role="tabpanel" id="language_banks_langs">
            @includeIf('admin.languages.relationships.languageBanksLangs', ['banksLangs' => $language->languageBanksLangs])
        </div>
        <div class="tab-pane" role="tabpanel" id="language_countries_langs">
            @includeIf('admin.languages.relationships.languageCountriesLangs', ['countriesLangs' => $language->languageCountriesLangs])
        </div>
        <div class="tab-pane" role="tabpanel" id="language_ref_family_status_langs">
            @includeIf('admin.languages.relationships.languageRefFamilyStatusLangs', ['refFamilyStatusLangs' => $language->languageRefFamilyStatusLangs])
        </div>
        <div class="tab-pane" role="tabpanel" id="language_nationality_langs">
            @includeIf('admin.languages.relationships.languageNationalityLangs', ['nationalityLangs' => $language->languageNationalityLangs])
        </div>
        <div class="tab-pane" role="tabpanel" id="language_ref_oked_langs">
            @includeIf('admin.languages.relationships.languageRefOkedLangs', ['refOkedLangs' => $language->languageRefOkedLangs])
        </div>
        <div class="tab-pane" role="tabpanel" id="language_ref_person_state_langs">
            @includeIf('admin.languages.relationships.languageRefPersonStateLangs', ['refPersonStateLangs' => $language->languageRefPersonStateLangs])
        </div>
        <div class="tab-pane" role="tabpanel" id="language_ref_vacation_types_langs">
            @includeIf('admin.languages.relationships.languageRefVacationTypesLangs', ['refVacationTypesLangs' => $language->languageRefVacationTypesLangs])
        </div>
    </div>
</div>

@endsection