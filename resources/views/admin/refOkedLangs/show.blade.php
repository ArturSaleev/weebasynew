@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.refOkedLang.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.ref-oked-langs.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.refOkedLang.fields.id') }}
                        </th>
                        <td>
                            {{ $refOkedLang->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.refOkedLang.fields.language') }}
                        </th>
                        <td>
                            {{ $refOkedLang->language->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.refOkedLang.fields.oked') }}
                        </th>
                        <td>
                            {{ $refOkedLang->oked->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.refOkedLang.fields.name_oked') }}
                        </th>
                        <td>
                            {{ $refOkedLang->name_oked }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.refOkedLang.fields.name') }}
                        </th>
                        <td>
                            {{ $refOkedLang->name }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.ref-oked-langs.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection