@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.refOkedLang.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.ref-oked-langs.update", [$refOkedLang->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="language_id">{{ trans('cruds.refOkedLang.fields.language') }}</label>
                <select class="form-control select2 {{ $errors->has('language') ? 'is-invalid' : '' }}" name="language_id" id="language_id">
                    @foreach($languages as $id => $entry)
                        <option value="{{ $id }}" {{ (old('language_id') ? old('language_id') : $refOkedLang->language->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('language'))
                    <div class="invalid-feedback">
                        {{ $errors->first('language') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.refOkedLang.fields.language_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="oked_id">{{ trans('cruds.refOkedLang.fields.oked') }}</label>
                <select class="form-control select2 {{ $errors->has('oked') ? 'is-invalid' : '' }}" name="oked_id" id="oked_id">
                    @foreach($okeds as $id => $entry)
                        <option value="{{ $id }}" {{ (old('oked_id') ? old('oked_id') : $refOkedLang->oked->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('oked'))
                    <div class="invalid-feedback">
                        {{ $errors->first('oked') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.refOkedLang.fields.oked_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="name_oked">{{ trans('cruds.refOkedLang.fields.name_oked') }}</label>
                <input class="form-control {{ $errors->has('name_oked') ? 'is-invalid' : '' }}" type="text" name="name_oked" id="name_oked" value="{{ old('name_oked', $refOkedLang->name_oked) }}">
                @if($errors->has('name_oked'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name_oked') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.refOkedLang.fields.name_oked_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="name">{{ trans('cruds.refOkedLang.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $refOkedLang->name) }}">
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.refOkedLang.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection