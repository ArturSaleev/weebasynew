@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.refVacationTypesLang.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.ref-vacation-types-langs.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="language_id">{{ trans('cruds.refVacationTypesLang.fields.language') }}</label>
                <select class="form-control select2 {{ $errors->has('language') ? 'is-invalid' : '' }}" name="language_id" id="language_id">
                    @foreach($languages as $id => $entry)
                        <option value="{{ $id }}" {{ old('language_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('language'))
                    <div class="invalid-feedback">
                        {{ $errors->first('language') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.refVacationTypesLang.fields.language_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="vacation_type_id">{{ trans('cruds.refVacationTypesLang.fields.vacation_type') }}</label>
                <select class="form-control select2 {{ $errors->has('vacation_type') ? 'is-invalid' : '' }}" name="vacation_type_id" id="vacation_type_id">
                    @foreach($vacation_types as $id => $entry)
                        <option value="{{ $id }}" {{ old('vacation_type_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('vacation_type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('vacation_type') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.refVacationTypesLang.fields.vacation_type_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="name">{{ trans('cruds.refVacationTypesLang.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}">
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.refVacationTypesLang.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection