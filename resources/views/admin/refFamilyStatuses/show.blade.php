@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.refFamilyStatus.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.ref-family-statuses.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.refFamilyStatus.fields.id') }}
                        </th>
                        <td>
                            {{ $refFamilyStatus->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.refFamilyStatus.fields.name') }}
                        </th>
                        <td>
                            {{ $refFamilyStatus->name }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.ref-family-statuses.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item">
            <a class="nav-link" href="#family_status_ref_family_status_langs" role="tab" data-toggle="tab">
                {{ trans('cruds.refFamilyStatusLang.title') }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" role="tabpanel" id="family_status_ref_family_status_langs">
            @includeIf('admin.refFamilyStatuses.relationships.familyStatusRefFamilyStatusLangs', ['refFamilyStatusLangs' => $refFamilyStatus->familyStatusRefFamilyStatusLangs])
        </div>
    </div>
</div>

@endsection