@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.mainCarousel.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.main-carousels.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.mainCarousel.fields.id') }}
                        </th>
                        <td>
                            {{ $mainCarousel->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mainCarousel.fields.title') }}
                        </th>
                        <td>
                            {{ $mainCarousel->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mainCarousel.fields.body') }}
                        </th>
                        <td>
                            {!! $mainCarousel->body !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mainCarousel.fields.visible') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled="disabled" {{ $mainCarousel->visible ? 'checked' : '' }}>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.main-carousels.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection