@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.refOked.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.ref-okeds.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.refOked.fields.id') }}
                        </th>
                        <td>
                            {{ $refOked->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.refOked.fields.oked') }}
                        </th>
                        <td>
                            {{ $refOked->oked }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.refOked.fields.name_oked') }}
                        </th>
                        <td>
                            {{ $refOked->name_oked }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.refOked.fields.name') }}
                        </th>
                        <td>
                            {{ $refOked->name }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.ref-okeds.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item">
            <a class="nav-link" href="#oked_ref_oked_langs" role="tab" data-toggle="tab">
                {{ trans('cruds.refOkedLang.title') }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" role="tabpanel" id="oked_ref_oked_langs">
            @includeIf('admin.refOkeds.relationships.okedRefOkedLangs', ['refOkedLangs' => $refOked->okedRefOkedLangs])
        </div>
    </div>
</div>

@endsection