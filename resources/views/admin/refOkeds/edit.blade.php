@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.refOked.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.ref-okeds.update", [$refOked->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="oked">{{ trans('cruds.refOked.fields.oked') }}</label>
                <input class="form-control {{ $errors->has('oked') ? 'is-invalid' : '' }}" type="text" name="oked" id="oked" value="{{ old('oked', $refOked->oked) }}">
                @if($errors->has('oked'))
                    <div class="invalid-feedback">
                        {{ $errors->first('oked') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.refOked.fields.oked_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="name_oked">{{ trans('cruds.refOked.fields.name_oked') }}</label>
                <input class="form-control {{ $errors->has('name_oked') ? 'is-invalid' : '' }}" type="text" name="name_oked" id="name_oked" value="{{ old('name_oked', $refOked->name_oked) }}">
                @if($errors->has('name_oked'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name_oked') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.refOked.fields.name_oked_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="name">{{ trans('cruds.refOked.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $refOked->name) }}">
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.refOked.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection