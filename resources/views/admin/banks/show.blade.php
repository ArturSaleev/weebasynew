@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.bank.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.banks.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.bank.fields.id') }}
                        </th>
                        <td>
                            {{ $bank->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bank.fields.name') }}
                        </th>
                        <td>
                            {{ $bank->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bank.fields.short_name') }}
                        </th>
                        <td>
                            {{ $bank->short_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bank.fields.logo') }}
                        </th>
                        <td>
                            {{ $bank->logo }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bank.fields.mfo') }}
                        </th>
                        <td>
                            {{ $bank->mfo }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bank.fields.mfo_head') }}
                        </th>
                        <td>
                            {{ $bank->mfo_head }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bank.fields.mfo_rkc') }}
                        </th>
                        <td>
                            {{ $bank->mfo_rkc }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bank.fields.kor_account') }}
                        </th>
                        <td>
                            {{ $bank->kor_account }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bank.fields.status') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled="disabled" {{ $bank->status ? 'checked' : '' }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bank.fields.bin') }}
                        </th>
                        <td>
                            {{ $bank->bin }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bank.fields.bik') }}
                        </th>
                        <td>
                            {{ $bank->bik }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bank.fields.commis') }}
                        </th>
                        <td>
                            {{ $bank->commis }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.banks.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item">
            <a class="nav-link" href="#bank_banks_langs" role="tab" data-toggle="tab">
                {{ trans('cruds.banksLang.title') }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" role="tabpanel" id="bank_banks_langs">
            @includeIf('admin.banks.relationships.bankBanksLangs', ['banksLangs' => $bank->bankBanksLangs])
        </div>
    </div>
</div>

@endsection