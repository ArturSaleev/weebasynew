@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.refFamilyStatusLang.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.ref-family-status-langs.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="language_id">{{ trans('cruds.refFamilyStatusLang.fields.language') }}</label>
                <select class="form-control select2 {{ $errors->has('language') ? 'is-invalid' : '' }}" name="language_id" id="language_id">
                    @foreach($languages as $id => $entry)
                        <option value="{{ $id }}" {{ old('language_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('language'))
                    <div class="invalid-feedback">
                        {{ $errors->first('language') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.refFamilyStatusLang.fields.language_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="family_status_id">{{ trans('cruds.refFamilyStatusLang.fields.family_status') }}</label>
                <select class="form-control select2 {{ $errors->has('family_status') ? 'is-invalid' : '' }}" name="family_status_id" id="family_status_id">
                    @foreach($family_statuses as $id => $entry)
                        <option value="{{ $id }}" {{ old('family_status_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('family_status'))
                    <div class="invalid-feedback">
                        {{ $errors->first('family_status') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.refFamilyStatusLang.fields.family_status_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="name">{{ trans('cruds.refFamilyStatusLang.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}">
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.refFamilyStatusLang.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection