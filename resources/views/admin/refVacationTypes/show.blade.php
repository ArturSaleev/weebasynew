@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.refVacationType.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.ref-vacation-types.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.refVacationType.fields.id') }}
                        </th>
                        <td>
                            {{ $refVacationType->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.refVacationType.fields.name') }}
                        </th>
                        <td>
                            {{ $refVacationType->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.refVacationType.fields.symbol') }}
                        </th>
                        <td>
                            {{ $refVacationType->symbol }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.refVacationType.fields.type') }}
                        </th>
                        <td>
                            {{ $refVacationType->type }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.ref-vacation-types.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item">
            <a class="nav-link" href="#vacation_type_ref_vacation_types_langs" role="tab" data-toggle="tab">
                {{ trans('cruds.refVacationTypesLang.title') }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" role="tabpanel" id="vacation_type_ref_vacation_types_langs">
            @includeIf('admin.refVacationTypes.relationships.vacationTypeRefVacationTypesLangs', ['refVacationTypesLangs' => $refVacationType->vacationTypeRefVacationTypesLangs])
        </div>
    </div>
</div>

@endsection