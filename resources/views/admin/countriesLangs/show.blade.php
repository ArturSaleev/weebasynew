@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.countriesLang.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.countries-langs.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.countriesLang.fields.id') }}
                        </th>
                        <td>
                            {{ $countriesLang->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.countriesLang.fields.language') }}
                        </th>
                        <td>
                            {{ $countriesLang->language->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.countriesLang.fields.country') }}
                        </th>
                        <td>
                            {{ $countriesLang->country->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.countriesLang.fields.name') }}
                        </th>
                        <td>
                            {{ $countriesLang->name }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.countries-langs.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection