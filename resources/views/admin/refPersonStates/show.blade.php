@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.refPersonState.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.ref-person-states.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.refPersonState.fields.id') }}
                        </th>
                        <td>
                            {{ $refPersonState->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.refPersonState.fields.name') }}
                        </th>
                        <td>
                            {{ $refPersonState->name }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.ref-person-states.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item">
            <a class="nav-link" href="#person_state_ref_person_state_langs" role="tab" data-toggle="tab">
                {{ trans('cruds.refPersonStateLang.title') }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" role="tabpanel" id="person_state_ref_person_state_langs">
            @includeIf('admin.refPersonStates.relationships.personStateRefPersonStateLangs', ['refPersonStateLangs' => $refPersonState->personStateRefPersonStateLangs])
        </div>
    </div>
</div>

@endsection